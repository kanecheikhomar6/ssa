import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static String sharedPreferencesUserKey = "USERKEY";
  // Seter token
  static Future<String> saveToken(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getToken() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('token');
  }

  static Future<String> saveUserId(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('userId', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getUserId() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('userId');
  }

  static Future<String> saveUserNom(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('nom', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getUserNom() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('nom');
  }

  static Future<String> saveUserEmail(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('email', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getUserEmail() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('email');
  }

  static Future<String> saveUserRole(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('role', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getUserRole() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('role');
  }

  static Future<String> saveCat(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('categorie', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getCat() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('categorie');
  }

  static Future<String> saveNiv(String dateFermeture) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('niveau', dateFermeture);
  }

  // Geter date de fermeture de la bande
  static Future<String> getNiv() async {
    final SharedPreferences pref = await SharedPreferences.getInstance();
    return pref.getString('niveau');
  }


}
