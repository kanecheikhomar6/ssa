import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ssa_quizz/preferences/preferences.dart';

import 'FirstPage.dart';
import 'Newcategorie.dart';
import 'bottomnavbar.dart';
import 'loader.dart';


class ProfilPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ProfilPageState();
  }
}

class _ProfilPageState extends State<ProfilPage> {

  String email = '', password = '', token, iduser;
  bool passwordVisible=false;
  var datas;
  var gamers;
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();



  bool loader = false;
  String nom;

  void initState() {
    setState(() {
      loader = true;
    });
    Preferences.getUserNom().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          nom = value;

        })
      }
    });

    Preferences.getUserId().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          iduser = value;
        }),
        print("hhhhhhhhhhhhhhhhhhhhh"),
        print(iduser),
        print("hhhhhhhhhhhhhhhhhhhhh"),
      }
    });

    Preferences.getToken().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          token = value;
          //loader = false;
        }),

        print("fffffffffffffffffffff"),
        print(token),
        print("fffffffffffffffffffff"),
        http.get(
          Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/gamers-global-ranking"),
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer "+token
          },
        ).then((http.Response response) {
          print(response.body);
          datas = json.decode(response.body);
          print(datas["data"]);
          gamers=datas["data"];

          setState(() {
            loader=false;
          });
        }),
      }
    });

    super.initState();
  }




  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        loader?Loading():Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0), // here the desired height
            child: AppBar(
              leading: GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BottomNav()),
                  );
                },
                child: Icon(Icons.chevron_left, color: Color(0xff676767), size: 30,),),
              centerTitle: true,
              title: Text(
                  "Profil",
                  style: const TextStyle(
                      color:  const Color(0xff676767),
                      fontWeight: FontWeight.w600,
                      fontFamily: "Rubik",
                      fontStyle:  FontStyle.normal,
                      fontSize: 18.0
                  ),
                  textAlign: TextAlign.left
              ),
              automaticallyImplyLeading: false,
              flexibleSpace: Image(
                image: AssetImage('assets/images/backgroundvierge.png'),
                fit: BoxFit.cover,
              ),

              backgroundColor: Colors.transparent,
            ),
          ),

          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: sizeX * .04),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  SizedBox(height: 70,),

                  Container(
                      child: Column(
                        children: [
                          Row(
                            children: [
                              Center(child: Container(
                                child: Center(
                                  child: Icon(Icons.person, size: 60, color: Colors.black,),
                                ),
                                width: sizeX*0.4,
                                height: 165,
                                //color: Colors.black,
                                decoration: BoxDecoration(
                                    border: Border.all(color: Colors.black),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10)
                                    ),
                                    color:  Color(0xffffffff),
                                ),
                              ),
                              ),
                              SizedBox(width: 20,),
                              Column(
                                children: [
                                  Text(
                                      nom,
                                      style: const TextStyle(
                                          color:  const Color(0xff676767),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 18.0
                                      ),
                                      textAlign: TextAlign.left
                                  )
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                      width: sizeX,
                      height: 165,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(10)
                          ),
                          color: const Color(0xffffffff)
                      ),
                  ),


                  SizedBox(height: 30,),

                  Text(
                      "Classement",
                      style: const TextStyle(
                          color:  const Color(0xff676767),
                          fontWeight: FontWeight.w400,
                          fontFamily: "Rubik",
                          fontStyle:  FontStyle.normal,
                          fontSize: 20.0
                      ),
                      textAlign: TextAlign.left
                  ),

                  SizedBox(height: 30,),

                  // Container(
                  //   child: Column(
                  //     children: [
                  //       Row(
                  //         children: [
                  //           Center(child: Container(
                  //             child: Center(
                  //               child: Icon(Icons.person, size: 40, color: Colors.black,),
                  //             ),
                  //             width: sizeX*0.2,
                  //             height: 65,
                  //             //color: Colors.black,
                  //             decoration: BoxDecoration(
                  //               border: Border.all(color: Colors.black),
                  //               borderRadius: BorderRadius.all(
                  //                   Radius.circular(10)
                  //               ),
                  //               color:  Color(0xffffffff),
                  //             ),
                  //           ),
                  //           ),
                  //           SizedBox(width: 20,),
                  //           Column(
                  //             children: [
                  //               Text(
                  //                   nom,
                  //                   style: const TextStyle(
                  //                       color:  const Color(0xff676767),
                  //                       fontWeight: FontWeight.w400,
                  //                       fontFamily: "Rubik",
                  //                       fontStyle:  FontStyle.normal,
                  //                       fontSize: 18.0
                  //                   ),
                  //                   textAlign: TextAlign.left
                  //               )
                  //             ],
                  //           ),
                  //         ],
                  //       ),
                  //     ],
                  //   ),
                  //   width: sizeX,
                  //   height: 65,
                  //   decoration: BoxDecoration(
                  //       borderRadius: BorderRadius.all(
                  //           Radius.circular(10)
                  //       ),
                  //       color: const Color(0xffffffff)
                  //   ),
                  // ),



                  GridView.count(
                    primary: false,
                    crossAxisCount: 2,
                    crossAxisSpacing: 2.0,
                    mainAxisSpacing: 2.0,
                    shrinkWrap: true,
                    children: List.generate(
                      //produitsLocaux.toSet().toList().length > 9 &&
                      //!voirPlusLocal
                      //? 9
                      //: produitsLocaux.toSet().toList().length,
                      gamers.length,
                          (index) {
                        return Container(
                          child: Column(
                            children: [
                              Row(
                                children: [
                                  Center(child: Container(
                                    child: Center(
                                      child: Icon(Icons.person, size: 40, color: Colors.black,),
                                    ),
                                    width: sizeX*0.45,
                                    height: 65,
                                    //color: Colors.black,
                                    decoration: BoxDecoration(
                                      border: Border.all(color: gamers[index]["gamer"]["nom_complet"]==nom?Colors.green:Colors.black),
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(10)
                                      ),
                                      color:  Color(0xffffffff),
                                    ),
                                  ),
                                  ),

                                ],
                              ),
                              SizedBox(height: 10,),
                              Text(
                                  gamers[index]["gamer"]["nom_complet"]==nom?"Moi":gamers[index]["gamer"]["nom_complet"],
                                  style:  TextStyle(
                                      color:  gamers[index]["gamer"]["nom_complet"]==nom ? Colors.green :  const Color(0xff676767),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),
                              SizedBox(height: 10,),

                              Text(
                                  (index+1).toString(),
                                  style: TextStyle(
                                      color:  gamers[index]["gamer"]["nom_complet"]==nom ? Colors.green :  const Color(0xff676767),
                                      fontWeight: FontWeight.w700,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 60.0
                                  ),
                                  textAlign: TextAlign.left
                              ),

                            ],
                          ),
                          width: sizeX,
                          height: 65,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10)
                              ),
                              color: const Color(0xffffffff)
                          ),
                        );
                      },
                    ),
                  ),





                ],
              ),

            ),
          ),
        )
    );
  }
}