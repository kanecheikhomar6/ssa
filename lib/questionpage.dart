import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:ssa_quizz/FirstPage.dart';
import 'package:toggle_bar/toggle_bar.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'dart:async';
import 'bottomnavbar.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

//
// class NavObject {
//   Widget screen;
//   Icon navIcon;
//   Text title;
//   NavObject({this.screen, this.navIcon, this.title});
// }
//
// List<NavObject> navItems = [
//   NavObject(
//     screen: CategoriePage(),
//     navIcon: Icon(Icons.home),
//     title: Text('Home'),
//   ),
//   NavObject(
//     screen: FirstPage(),
//     navIcon: Icon(Icons.settings),
//     title: Text('Settings'),
//   ),
//   NavObject(
//     screen: CategoriePage(),
//     navIcon: Icon(Icons.share),
//     title: Text('Share'),
//   ),
// ];

class QuestionPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _QuestionPageState();
  }
}

class _QuestionPageState extends State<QuestionPage> {
  YoutubePlayerController _controller;


  List<String> labels = ["Q1","Q2","Q3","Q4","Q5","Q6"];
  int counter = 0;

  Timer timer;
  int start = 120;

  bool choix13 = false;
  bool choix12 = false;
  bool choix11 = false;

  bool choix23 = false;
  bool choix22 = false;
  bool choix21 = false;

  bool choix33 = false;
  bool choix32 = false;
  bool choix31 = false;

  bool choix43 = false;
  bool choix42 = false;
  bool choix41 = false;

  bool choix54 = false;
  bool choix53 = false;
  bool choix52 = false;
  bool choix51 = false;

  bool choix64 = false;
  bool choix63 = false;
  bool choix62 = false;
  bool choix61 = false;

  bool finish = false;

  bool q1 = true;
  bool q2 = false;
  bool q3 = false;
  bool q4 = false;
  bool q5 = false;
  bool q6 = false;

  void startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      setState(() {
        start--;
        if(start < 100 && start >79){
          q1=false;
          q2=true;
          q3=false;
          q4=false;
          q5=false;
          q6=false;
        }

        if(start < 80 && start >59){
          q1=false;
          q2=false;
          q3=true;
          q4=false;
          q5=false;
          q6=false;
        }

        if(start < 60 && start >39){
          q1=false;
          q2=false;
          q3=false;
          q4=true;
          q5=false;
          q6=false;
        }

        if(start < 40 && start >19){
          q1=false;
          q2=false;
          q3=false;
          q4=false;
          q5=true;
          q6=false;
        }

        if(start < 20 && start >0){
          q1=false;
          q2=false;
          q3=false;
          q4=false;
          q5=false;
          q6=true;
        }

        if(start==0 &&!finish){
          _settingModalBottomSheet(context);
        }

        if(finish && start >0){
          start=0;
        }

        if(start==0){
            finish=true;
            q1=true;
            q2=false;
            q3=false;
            q4=false;
            q5=false;
            q6=false;
        }

      });
    });
  }

  @override
  void initState() {
    startTimer();
    _controller = YoutubePlayerController(
      initialVideoId: 't8yCimVcer4', // id youtube video
      flags: YoutubePlayerFlags(
        autoPlay: false,
        mute: false,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
      ),
    );
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return (
        Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0), // here the desired height
              child: AppBar(
                leading: Icon(Icons.chevron_left, color: Color(0xff676767), size: 30,),
                centerTitle: true,
                title: Text(
                    "Hygiéne personnelle",
                    style: const TextStyle(
                        color:  const Color(0xff676767),
                        fontWeight: FontWeight.w600,
                        fontFamily: "Rubik",
                        fontStyle:  FontStyle.normal,
                        fontSize: 18.0
                    ),
                    textAlign: TextAlign.left
                ),
                automaticallyImplyLeading: false,
                flexibleSpace: Image(
                  image: AssetImage('assets/images/backgroundvierge.png'),
                  fit: BoxFit.cover,
                ),

                backgroundColor: Colors.transparent,
              ),
            ),
            body: SingleChildScrollView(
              child:
              Container(
                child: Column(
                    children:[

                // Center(
                // child: Container(
                // width: sizeX,
                //     child: ToggleBar(
                //        labels: labels,
                //         textColor: Color(0xff000000),
                //         backgroundColor: Color(0xfff0f0f0),
                //         selectedTabColor: Color(0xff676767),
                //         selectedTextColor: Colors.white,
                //         labelTextStyle: TextStyle(
                //             color:  const Color(0xff000000),
                //             fontWeight: FontWeight.w400,
                //             fontFamily: "Rubik",
                //             fontStyle:  FontStyle.normal,
                //             fontSize: 16.0
                //         ),
                //         onSelectionUpdated: (index){
                //          setState(() {
                //                  counter = index;
                //          });
                //         },
                //       ),
                // ),
                // ),
                //
                      SizedBox(height: 30,),

                      Container(
                        child: Center(
                          child: Row(

                            children: [
                              SizedBox(width: 8,),
                              Icon(Icons.access_time, color: Color(0xff707070), size: 20),
                              SizedBox(width: 8,),
                              Text(
                                  start<0 ? "Ecoulé": start.toString()+" "+"sec",
                                  style: const TextStyle(
                                      color:  const Color(0xff676767),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),

                            ],
                          ),
                        ),
                          width: 120,
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10)
                              ),
                              border: Border.all(
                                  color: const Color(0xff707070),
                                  width: 1
                              ),
                              color: const Color(0xffffffff)
                          ),
                      ),

                      SizedBox(height: 30,),

                      Center(
                        child: Container(
                          width: sizeX*0.9,
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                            GestureDetector(
                              onTap: (){
                              setState(() {
                                q1 = true;
                                q2 = false;
                                q3 = false;
                                q4 = false;
                                q5 = false;
                                q6 = false;
                              });
                              },
                              child: Container(
                              child: Center(child:Text(
                                  "Q1",
                                  style: TextStyle(
                                      color: q1 ? Colors.white : Color(0xff000000),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),),
                              decoration: BoxDecoration(
                                color: q1 ? Color(0xff676767) : Color(0xfff0f0f0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 40,
                              width: 40,
                            ),),

                            GestureDetector(
                              onTap: (){
                                setState(() {
                                  q1 = false;
                                  q2 = true;
                                  q3 = false;
                                  q4 = false;
                                  q5 = false;
                                  q6 = false;
                                });
                              },
                              child: Container(
                              child: Center(child:Text(
                                  "Q2",
                                  style: TextStyle(
                                      color: q2 ? Colors.white : Color(0xff000000),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),),
                              decoration: BoxDecoration(
                                color: q2 ? Color(0xff676767) : Color(0xfff0f0f0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 40,
                              width: 40,
                            ),),

                            GestureDetector(
                              onTap: (){
                                setState(() {
                                  q1 = false;
                                  q2 = false;
                                  q3 = true;
                                  q4 = false;
                                  q5 = false;
                                  q6 = false;
                                });
                              },
                              child: Container(
                              child: Center(child:Text(
                                  "Q3",
                                  style: TextStyle(
                                      color: q3 ? Colors.white : Color(0xff000000),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),),
                              decoration: BoxDecoration(
                                color: q3 ? Color(0xff676767) : Color(0xfff0f0f0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 40,
                              width: 40,
                            ),),

                            GestureDetector(
                              onTap: (){
                                setState(() {
                                  q1 = false;
                                  q2 = false;
                                  q3 = false;
                                  q4 = true;
                                  q5 = false;
                                  q6 = false;
                                });
                              },
                              child: Container(
                              child: Center(child:Text(
                                  "Q4",
                                  style: TextStyle(
                                      color: q4 ? Colors.white : Color(0xff000000),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),),
                              decoration: BoxDecoration(
                                color: q4 ? Color(0xff676767) : Color(0xfff0f0f0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 40,
                              width: 40,
                            ),),

                            GestureDetector(
                              onTap: (){
                                setState(() {
                                  q1 = false;
                                  q2 = false;
                                  q3 = false;
                                  q4 = false;
                                  q5 = true;
                                  q6 = false;
                                });
                              },
                              child: Container(
                              child: Center(child:Text(
                                  "Q5",
                                  style: TextStyle(
                                      color:  q5 ? Colors.white : Color(0xff000000),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),),
                              decoration: BoxDecoration(
                                color: q5 ? Color(0xff676767) : Color(0xfff0f0f0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 40,
                              width: 40,
                            ),),

                            GestureDetector(
                              onTap: (){
                                setState(() {
                                  q1 = false;
                                  q2 = false;
                                  q3 = false;
                                  q4 = false;
                                  q5 = false;
                                  q6 = true;
                                });
                              },
                              child: Container(
                              child: Center(child:Text(
                                  "Q6",
                                  style: TextStyle(
                                      color: q6 ? Colors.white : Color(0xff000000),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),),
                              decoration: BoxDecoration(
                                color: q6 ? Color(0xff676767) : Color(0xfff0f0f0),
                                borderRadius: BorderRadius.circular(10),
                              ),
                              height: 40,
                              width: 40,
                            ),),



                          ],),
                        ),
                      ),


                      SizedBox(height: 30,),


                      Row(children: [
                        SizedBox(width: 20,), Text(
                            "Question",
                            style: const TextStyle(
                                color:  const Color(0xff676767),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 20.0
                            ),
                            textAlign: TextAlign.left
                        ),
                      ],),

                      SizedBox(height: 15,),

                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                              color: Color(0xfff0f0f0),
                              borderRadius: BorderRadius.circular(10),
                          ),
                          width: sizeX*0.9,
                          height: 70,
                          child: Center(
                            child: Text(
                              q1 ? "Quelles sont les étapes pour un lavage efficace des mains?":q2?"Pour bien exercer sa profession, la tenue doit être :":q3?"Qu'est-ce que l'hygiène ?":q4 ? "Quelles sont les personnes qui risquent de développer des maladies d'origine alimentaire ?" : q5 ? "Quelles sont les précautions standards ?" : "Quelle est l'utilité du nettoyage ?",
                          style: const TextStyle(
                              color:  const Color(0xff000000),
                              fontWeight: FontWeight.w400,
                              fontFamily: "Rubik",
                              fontStyle:  FontStyle.normal,
                              fontSize: 12.0
                          ),
                            textAlign: TextAlign.left
                        ),),
                        ),
                      ),

                      SizedBox(height: 20,),

                      Row(children: [
                        SizedBox(width: 20,), Text(
                            finish?"Correction":"Réponses",
                            style: const TextStyle(
                                color:  const Color(0xff676767),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 20.0
                            ),
                            textAlign: TextAlign.left
                        ),
                      ],),

                      SizedBox(height: 20,),




                      //QUESTION 1

                      q1&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix11 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix11?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 85,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix11 = !choix11;
                                });
                              },
                              title: Text("Mouillez-vous les mains - faites mousser avec du savon en frottant  vos mains ensemble - frotter pendant 20 secondes - rincer à l’eau - sécher avec une serviette propre.", style: TextStyle(
                                  color:  choix11? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix11 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):finish &&q1?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xff86c400),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 85,
                          child: Center(
                            child: ListTile(
                              title: Text("Mouillez-vous les mains - faites mousser avec du savon en frottant  vos mains ensemble - frotter pendant 20 secondes - rincer à l’eau - sécher avec une serviette propre.", style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix11 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q1?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q1&&!finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix12 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix12?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 57,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix12 = !choix12;
                                });
                              },
                              title: Text("Faites mousser vos mains avec du savons - rincer à l’eau - sécher avec une serviette propre", style: TextStyle(
                                  color: choix12 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix12 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q1&&finish?
                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix12 ? Colors.red:Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 57,
                          child: Center(
                            child: ListTile(
                              title: Text("Faites mousser vos mains avec du savons - rincer à l’eau - sécher avec une serviette propre", style: TextStyle(
                                  color: choix12 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix12 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),

                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      //SizedBox(height: 20,),
                      q1?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q1&&!finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix13 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix13?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix13 = !choix13;
                                });
                              },
                              title: Text('Rincer à l’eau claire - sécher avec une serviette propre', style: TextStyle(
                                  color: choix13 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                                ),
                              trailing: choix13 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ): q1&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix13?Colors.red:Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text('Rincer à l’eau claire - sécher avec une serviette propre', style: TextStyle(
                                  color: choix13?Colors.white:Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix13 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q1&&finish?SizedBox(height: 20,):SizedBox(height: 0.005,),

                      q1&&finish? Container(
                        width: sizeX*0.9,
                        child: YoutubePlayer(
                        controller: YoutubePlayerController(
                          initialVideoId: 't8yCimVcer4', //Add videoID.
                          flags: YoutubePlayerFlags(
                            hideControls: false,
                            controlsVisibleAtStart: true,
                            autoPlay: false,
                            mute: false,
                          ),
                        ),
                        showVideoProgressIndicator: true,
                        progressIndicatorColor: Colors.blue,
                      ),):SizedBox(height: 0.005,),

                      //QUESTION 2

                      q2&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix21 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix21?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix21 = !choix21;
                                });
                              },
                              title: Text("Ongles coupés et propres", style: TextStyle(
                                  color:  choix21? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix21 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xff86c400),
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Ongles coupés et propres", style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix21 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q2?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q2&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix22 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix22?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix22 = !choix22;
                                });
                              },
                              title: Text("Bijoux aux mains", style: TextStyle(
                                  color:  choix22? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix22 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix22 ? Colors.red : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Bijoux aux mains", style: TextStyle(
                                  color: choix22 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix22 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q2?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q2&&!finish? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix23 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix23?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix23 = !choix23;
                                });
                              },
                              title: Text("Cheveux longs possible mais avec un couvre chef", style: TextStyle(
                                  color:  choix23 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix23 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix23 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Cheveux longs possible mais avec un couvre chef", style: TextStyle(
                                  color:  choix23? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix23 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q2&&finish?SizedBox(height: 20,):SizedBox(height: 0.005,),

                      q2&&finish? Container(
                        width: sizeX*0.9,
                        child: YoutubePlayer(
                          controller: YoutubePlayerController(
                            initialVideoId: 'Rk2AmJwCIyo', //Add videoID.
                            flags: YoutubePlayerFlags(
                              hideControls: false,
                              controlsVisibleAtStart: true,
                              autoPlay: false,
                              mute: false,
                            ),
                          ),
                          showVideoProgressIndicator: true,
                          progressIndicatorColor: Colors.blue,
                        ),):SizedBox(height: 0.005,),









                      //QUESTION 3

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix31 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix31?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix31 = !choix31;
                                });
                              },
                              title: Text("Ensemble de pratiques pour se protéger des bactéries", style: TextStyle(
                                  color:  choix31? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix31 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix31 ? Colors.red : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Ensemble de pratiques pour se protéger des bactéries", style: TextStyle(
                                  color:  choix31? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix31 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q3?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix32 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix32?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix32 = !choix32;
                                });
                              },
                              title: Text("Ensemble des règles à suivre pour la conservation de la santé", style: TextStyle(
                                  color:  choix32? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                               ),
                              trailing: choix32 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix32 ? Colors.red : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Ensemble des règles à suivre pour la conservation de la santé", style: TextStyle(
                                  color: choix32 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix32 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q3?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix33 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix33?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix33 = !choix33;
                                });
                              },
                              title: Text("Principes et pratiques qui visent à préserver et à favoriser la santé par des moyens individuels ou collectifs", style: TextStyle(
                                  color:  choix33? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix33 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix33 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Principes et pratiques qui visent à préserver et à favoriser la santé par des moyens individuels ou collectifs", style: TextStyle(
                                  color:  choix33? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix33 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),







                      //QUESTION 4

                      q4&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix41 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix41?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 68,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix41 = !choix41;
                                });
                              },
                              title: Text("Bébés, jeunes enfants, femmes enceintes, personnes âgées, personnes affectées par un cancer ou le sida", style: TextStyle(
                                  color:  choix41? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix41 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q4&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix41 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 68,
                          child: Center(
                            child: ListTile(
                              title: Text("Bébés, jeunes enfants, femmes enceintes, personnes âgées, personnes affectées par un cancer ou le sida", style: TextStyle(
                                  color:  choix41? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix41 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q4?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q4&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix42 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix42?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix42 = !choix42;
                                });
                              },
                              title: Text("Sportifs, personnes maniaques, cyclistes, docteurs, infirmiers", style: TextStyle(
                                  color:  choix42 ? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                               ),
                              ),
                              trailing: choix42 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q4&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix42 ? Colors.red : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Sportifs, personnes maniaques, cyclistes, docteurs, infirmiers", style: TextStyle(
                                  color:  choix42 ? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix42 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

















                      //QUESTION 5

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix51 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix51?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix51 = !choix51;
                                });
                              },
                              title: Text("L'hygiène des mains", style: TextStyle(
                                  color:  choix51? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix51 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix51 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("L'hygiène des mains", style: TextStyle(
                                  color:  choix51? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix51 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q5?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix52 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix52?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix52 = !choix52;
                                });
                              },
                              title: Text("Le lavage des pieds", style: TextStyle(
                                  color:  choix52? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix52 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix52 ? Colors.red : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Le lavage des pieds", style: TextStyle(
                                  color:  choix52? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix52 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q5?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix53 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix53?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix53 = !choix53;
                                });
                              },
                              title: Text("La tenue de protection", style: TextStyle(
                                  color:  choix53? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix53 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix53 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("La tenue de protection", style: TextStyle(
                                  color:  choix53? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix53 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q5?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix54 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix54?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix54 = !choix54;
                                });
                              },
                              title: Text("Le port de gants", style: TextStyle(
                                  color:  choix54? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix54 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix54 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Le port de gants", style: TextStyle(
                                  color:  choix54? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix54 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),












                      //QUESTION 6

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix61 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix61?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix61 = !choix61;
                                });
                              },
                              title: Text("Débarrasser les salissures", style: TextStyle(
                                  color:  choix61? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix61 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix61 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Débarrasser les salissures", style: TextStyle(
                                  color:  choix61? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix61 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q6?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix62 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix62?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix62 = !choix62;
                                });
                              },
                              title: Text("Diminuer les contaminations", style: TextStyle(
                                  color:  choix62? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix62 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix62 ? Color(0xff86c400) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xff86c400),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("Diminuer les contaminations", style: TextStyle(
                                  color:  choix62? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix62 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q6?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix63 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix63?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix63 = !choix63;
                                });
                              },
                              title: Text("D'avoir une maison propre", style: TextStyle(
                                  color:  choix63? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix63 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix63 ? Colors.red : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text("D'avoir une maison propre", style: TextStyle(
                                  color:  choix63? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix63 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      SizedBox(height: 25,),


                      q6&&!finish? GestureDetector(
                          onTap: (){
                            setState(() {
                              finish = true;
                              _settingModalBottomSheet(context);
                            });

                          },
                          child: Center(child: Container(
                          child:
                          Center(child: Text(
                            "Terminer",
                            style: const TextStyle(
                                color:  const Color(0xffffffff),
                                fontWeight: FontWeight.w600,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 30.0
                            ),
                          ),
                          ),
                          width: 285,
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10)
                              ),
                              color: const Color(0xff676767)
                          )
                      ))):q6&&finish?SizedBox(height: 1,):SizedBox(height: 1,),


                      q6&&finish? GestureDetector(
                          onTap: (){},
                          child: Center(child: Container(
                              child:
                              Center(child: Text(
                                "Retour",
                                style: const TextStyle(
                                    color:  const Color(0xffffffff),
                                    fontWeight: FontWeight.w600,
                                    fontFamily: "Rubik",
                                    fontStyle:  FontStyle.normal,
                                    fontSize: 30.0
                                ),
                              ),
                              ),
                              width: 285,
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10)
                                  ),
                                  color: const Color(0xff676767),
                              )
                          ))):SizedBox(height: 1,),

                      SizedBox(height: 25,),



                    ]),
              ),
            )));
  }

  void _settingModalBottomSheet(context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    final text =
        "Pour chaque thème choisi, vous aurez 10 niveaux.\nPour chaque niveau vous aurez 15 questions et 3 coeurs(vies).\n\nSi vous epuisez vos 3 vies pendant la partie, \nune vidéo se lancera automatiquement.\nRegardez-la jusqu'à la fin pour gagner une vie ou\nquittez-la pour terminer la partie.\n\nPour un thème, vous gagnerez une medaille d'or, \nd'argent ou de bronze si vous accomplissez \nles 3 derniers niveaux.";

    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
              heightFactor: 0.745,
              child: Container(
                height: 900,
                child: new Wrap(
                  children: <Widget>[
                    new ListTile(
                      trailing: new GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                          q1=true;
                          q2=false;
                          q3=false;
                          q4=false;
                          q5=false;
                          q6=false;
                        },
                        child: Icon(
                          Icons.close,
                          size: 30,
                          color: Color(0xff676767),
                        ),
                      ),
                      title: Row(
                        children: [
                          new Text(
                            "Votre score",
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xff676767),
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),
                    ),

                    SizedBox(
                      height: 50,
                    ),
                    Center(child:Container(
                        child: Center(child: // 75
                        Text(
                            "75",
                            style: const TextStyle(
                                color:  const Color(0xff676767),
                                fontWeight: FontWeight.w400,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 100.0
                            ),
                            textAlign: TextAlign.left
                        ),

                        ),
                        width: 225,
                        height: 135,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)
                            ),
                            border: Border.all(
                                color: const Color(0xff676767),
                                width: 1
                            )
                        ),
                    ),
                    ),

                    SizedBox(height: 30,),

                    Center(child: Container(
                      child: Center(child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Center(
                                child: Column(children:[
                                  Text(
                                      "06",
                                      style: const TextStyle(
                                          color:  const Color(0xff676767),
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),

                                  // Questions
                                  Text(
                                      "Questions",
                                      style: const TextStyle(
                                          color:  const Color(0xff676767),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 10.0
                                      ),
                                      textAlign: TextAlign.left
                                  )
                                ],)
                            ),
                              width: 70,
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(5)
                                  ),
                                  border: Border.all(
                                      color: const Color(0xff676767),
                                      width: 1
                                  ),
                              ),
                          ),
                          Container(
                            child: Center(
                                child: Column(children:[
                                  Text(
                                      "04",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.w600,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),

                                  // Questions
                                  Text(
                                      "Questions",
                                      style: const TextStyle(
                                          color:  Colors.white,
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 10.0
                                      ),
                                      textAlign: TextAlign.left
                                  )
                                ],)
                            ),
                              width: 70,
                              height: 45,
                              decoration: BoxDecoration(
                                color: Color(0xff86c400),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(5)
                                  ),
                                  border: Border.all(
                                      color: const Color(0xff86c400),
                                      width: 1
                                  ),
                              ),
                          ),
                          Container(
                            child: Center(
                              child: Column(children:[
                                Text(
                                    "02",
                                    style: TextStyle(
                                        color:  Colors.white,
                                        fontWeight: FontWeight.w600,
                                        fontFamily: "Rubik",
                                        fontStyle:  FontStyle.normal,
                                        fontSize: 16.0
                                    ),
                                    textAlign: TextAlign.left
                                ),

                                // Questions
                                Text(
                                    "incorrectes",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "Rubik",
                                        fontStyle:  FontStyle.normal,
                                        fontSize: 10.0
                                    ),
                                    textAlign: TextAlign.left
                                )
                              ],)
                            ),
                              width: 70,
                              height: 45,
                              decoration: BoxDecoration(
                                color: Color(0xfffb0015),
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(5)
                                  ),
                                  border: Border.all(
                                      color: const Color(0xfffb0015),
                                      width: 1
                                  )
                              )
                          ),
                        ],
                      ),),
                      height: 120,
                      color: Colors.transparent,
                      width: sizeX*0.8,
                    )),

                    Center(
                      child:Container(
                        child: Center(
          child: // Jouer encore
          Text(
              "Correction",
              style: const TextStyle(
                  color:  const Color(0xffffffff),
                  fontWeight: FontWeight.w600,
                  fontFamily: "Rubik",
                  fontStyle:  FontStyle.normal,
                  fontSize: 26.0
              ),
              textAlign: TextAlign.left
          )),
                        width: sizeX*0.75,
                        height: 45,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)
                            ),
                            color: const Color(0xff707070)
                        )
                    ),
                    ),


                  ],
                ),
              ));
        });
  }


}




