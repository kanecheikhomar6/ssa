import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ssa_quizz/loader.dart';
import 'package:ssa_quizz/preferences/preferences.dart';

import 'FirstPage.dart';
import 'Newcategorie.dart';
import 'bottomnavbar.dart';


class ClassementPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ClassementPageState();
  }
}

class _ClassementPageState extends State<ClassementPage> {

  String email = '', password = '', token='', iduser='', nom;
  var datas;
  bool passwordVisible=false;
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();



  bool loader = false;
  bool ok;
  bool clicked = false;
  bool error = false;


  void initState() {
    setState(() {
      loader = true;
    });
    Preferences.getUserNom().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          nom = value;

        })
      }
    });

    Preferences.getUserId().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          iduser = value;
        }),
        print("hhhhhhhhhhhhhhhhhhhhh"),
        print(iduser),
        print("hhhhhhhhhhhhhhhhhhhhh"),
      }
    });

    Preferences.getToken().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          token = value;
          //loader = false;
        }),

        print("fffffffffffffffffffff"),
        print(token),
        print("fffffffffffffffffffff"),
        http.get(
          Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/badges-by-gamer/"+iduser),
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer "+token
          },
        ).then((http.Response response) {
          //print(response.body);
          datas = json.decode(response.body);
          print(datas);
          print(datas["data"]);
          //gamers=datas["data"];

          setState(() {
            loader = false;
          });

        }),
      }
    });

    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        loader?Loading():Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(60.0), // here the desired height
            child: AppBar(
              leading: GestureDetector(
                onTap: (){
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => BottomNav()),
                  );
                },
                child: Icon(Icons.chevron_left, color: Color(0xff676767), size: 30,),),
              centerTitle: true,
              title: Text(
                  "Mes trophées",
                  style: const TextStyle(
                      color:  const Color(0xff676767),
                      fontWeight: FontWeight.w600,
                      fontFamily: "Rubik",
                      fontStyle:  FontStyle.normal,
                      fontSize: 18.0
                  ),
                  textAlign: TextAlign.left
              ),
              automaticallyImplyLeading: false,
              flexibleSpace: Image(
                image: AssetImage('assets/images/backgroundvierge.png'),
                fit: BoxFit.cover,
              ),

              backgroundColor: Colors.transparent,
            ),
          ),

          body: SingleChildScrollView(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: sizeX * .04),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [

                  SizedBox(height: 20,),
                  datas["count"]==0?Center(child: Text("Vous n'avez aucun trophée actuellement", style: TextStyle(color: Colors.red, fontSize: 17))):GridView.count(
                    primary: false,
                    crossAxisCount: 2,
                    crossAxisSpacing: 2.0,
                    mainAxisSpacing: 2.0,
                    shrinkWrap: true,
                    children: List.generate(
                      //produitsLocaux.toSet().toList().length > 9 &&
                      //!voirPlusLocal
                      //? 9
                      //: produitsLocaux.toSet().toList().length,
                      datas["count"],
                          (index) {
                        return GestureDetector(
                          onTap: (){},
                          child: Column( children:  <Widget>[ Container(
                            decoration: BoxDecoration(
                              image: DecorationImage(
                                  fit: BoxFit.fill,
                                  image: AssetImage("assets/images/trophy.png"),
                              ),
                              borderRadius: BorderRadius.only(
                                topRight: Radius.circular(30.0),
                                bottomLeft: Radius.circular(30.0),
                              ),
                            ),
                            width: sizeX*0.4,
                            height: sizeX*0.4,
                            //color: Colors.amber,
                          ),
                            SizedBox(height: 5,),
                            Text("",  style: const TextStyle(
                                color:  const Color(0xff0f0f0f),
                                fontWeight: FontWeight.w400,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 10.0
                            ),),

                          ],
                          ),
                        );
                      },
                    ),
                  ),

                ],
              ),

            ),
          ),
        )
    );
  }
}