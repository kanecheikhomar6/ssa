import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ssa_quizz/preferences/preferences.dart';

import 'FirstPage.dart';
import 'Newcategorie.dart';
import 'bottomnavbar.dart';


class LoginPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _LoginPageState();
  }
}

class _LoginPageState extends State<LoginPage> {

  String email = '', password = '';
  bool passwordVisible=false;
  TextEditingController _email = TextEditingController();
  TextEditingController _password = TextEditingController();



  bool loader = false;
  bool ok;
  bool clicked = false;
  bool error = false;


  void connexion() {

    setState(() {
      loader=true;
      clicked = true;
    });

    http.post(
        Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/login"),
        headers: {"Content-Type": "application/json"},
        body: json.encode({
          "login": email,
          "password": password
        }))
        .then((http.Response response) {
      print(response.statusCode);
      print(response.body);
      var datas = json.decode(response.body);
      print(datas["access_token"]);
      print("fffffffffffffffffff");
      print(datas["user"]["email"]);
      print("fffffffffffffffffff");
      // voir si le document n'existe pas déjà pour lever cette exception
      // To do later
      if (response.statusCode == 200) {
        Preferences.saveToken(datas["access_token"]);
        Preferences.saveUserId(datas["user"]["user_id"]);
        Preferences.saveUserNom(datas["user"]["nom_complet"]);
        Preferences.saveUserEmail(datas["user"]["email"]);
        Preferences.saveUserRole(datas["user"]["role"]);
        Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => BottomNav()),
        );
        setState(() {
          ok=true;
        });
        print("ok");
      }
      // else{
      //  print("pas bon");
      //
      //  setState(() {
      //    error=true;
      //  });
      // }
    }).catchError((erreur) {
      print("ddddddddddddddd");
      print(erreur);
      print("ddddddddddddddd");
       setState(() {
         error=true;
       });
    });
  }
  
  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(190.0), // here the desired height
              child: AppBar(
                automaticallyImplyLeading: false,
                flexibleSpace: Image(
                  image: AssetImage('assets/images/logo.png'),
                  fit: BoxFit.cover,
                ),

                backgroundColor: Colors.transparent,
              ),
            ),

            body: SingleChildScrollView(
              child: Padding(
              padding: EdgeInsets.symmetric(horizontal: sizeX * .04),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
              children: [

                SizedBox(height: 70,),

                Text(
                    "Email",
                    style: const TextStyle(
                        color:  const Color(0xff676767),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Rubik",
                        fontStyle:  FontStyle.normal,
                        fontSize: 16.0
                    ),
                    textAlign: TextAlign.left
                ),


                SizedBox(height: 20,),

                TextFormField(
                  keyboardType: TextInputType.text,
                  style: TextStyle(color: Colors.grey),
                  cursorColor: Colors.green,
                  controller: _email,
                  validator: (val) => (val.isEmpty)
                      ? 'Veuillez saisir un nom d\'utilisateur correct'
                      : null,
                  decoration: InputDecoration(

                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.green,
                        width: 1.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    hintText: "Email",
                    hintStyle: TextStyle(
                      color: Color(0xff696969),
                      fontSize: 14,
                    ),
                  ),
                  onChanged: (val) {
                    setState(() {
                      email = val;
                    });
                    print(email);
                  },
                ),

                SizedBox(
                  height: 20,
                ),

                Text(
                    "Mot de passe",
                    style: const TextStyle(
                        color:  const Color(0xff676767),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Rubik",
                        fontStyle:  FontStyle.normal,
                        fontSize: 16.0
                    ),
                    textAlign: TextAlign.left
                ),

                SizedBox(
                  height: 20,
                ),

                TextFormField(
                  obscuringCharacter: "✱",
                  validator: (val) => (val.isEmpty)
                      ? 'Votre mot de passe est requis'
                      : null,
                  controller: _password,
                  obscureText: (passwordVisible) ? false : true,
                  keyboardType: TextInputType.text,
                  textCapitalization: TextCapitalization.sentences,
                  style: TextStyle(color: Colors.grey),
                  cursorColor: Colors.green,
                  onChanged: (val) {
                    setState(() {
                      password = val;
                    });
                    print(password);
                    // if (val.length >= 6) {
                    //   setState(() {
                    //     ok = true;
                    //   });
                    // } else {
                    //   setState(() {
                    //     ok = false;
                    //   });
                    // }
                  },
                  decoration: InputDecoration(

                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.green,
                        width: 1.0,
                      ),
                    ),
                    enabledBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(10),
                      borderSide: BorderSide(
                        color: Colors.grey,
                      ),
                    ),
                    hintText: "Mot de passe",
                    hintStyle: TextStyle(
                      color: Color(0xff696969),
                      fontSize: 14,
                    ),
                  ),
                ),
                SizedBox(height: 50,),

                // Button se connecter
                GestureDetector(
                  onTap: (){
                    connexion();
                    // Navigator.push(
                    //   context,
                    //   MaterialPageRoute(builder: (context) => LoginPage()),
                    // );
                  },
                  child: Container(
                    child: // Se connecter
                    Center(child: Text(
                        "Se connecter",
                        style: const TextStyle(
                            color:  const Color(0xffffffff),
                            fontWeight: FontWeight.w600,
                            fontFamily: "Rubik",
                            fontStyle:  FontStyle.normal,
                            fontSize: 26.0
                        ),
                        textAlign: TextAlign.left
                    ),
                    ),
                    //width: sizeX*0.8,
                    height: 55,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(10),
                      ),
                      color: const Color(0xff2bca00),
                    ),
                  ),),


                SizedBox(height: 20,),


                GestureDetector(
                    onTap: (){
                      Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => FirstPage()),
                      );
                    },
                    child: Container(
                        child: Center(child: // Commencer
                        Text(
                            "Retour",
                            style: const TextStyle(
                                color:  const Color(0xffffffff),
                                fontWeight: FontWeight.w600,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 26.0
                            ),
                            textAlign: TextAlign.left
                        ),),
                        //width: 285,
                        height: 55,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)
                            ),
                            color: const Color(0xff707070)
                        )
                    )
                ),

                SizedBox(height: 20,),

                    error?Text(
                    "Email ou mot de passe incorrects",
                    style: const TextStyle(
                        color: Colors.red,
                        fontWeight: FontWeight.w400,
                        fontFamily: "Rubik",
                        fontStyle:  FontStyle.normal,
                        fontSize: 16.0
                    ),
                    textAlign: TextAlign.left
                ):SizedBox.shrink(),


              ],
            ),

        ),
        ),
        )
            );
  }
}