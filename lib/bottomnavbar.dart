import 'package:flutter/material.dart';
import 'package:ssa_quizz/Newcategorie.dart';
import 'package:ssa_quizz/profil.dart';

import 'CategoriePage.dart';
import 'FirstPage.dart';
import 'classement.dart';

class NavObject {
  Widget screen;
  Icon navIcon;
  Text title;
  NavObject({this.screen, this.navIcon, this.title});
}

Color HexaColor(String strcolor, {int opacity = 15}){ //opacity is optional value
  strcolor = strcolor.replaceAll("#", ""); //replace "#" with empty value
  String stropacity = opacity.toRadixString(16); //convert integer opacity to Hex String
  return Color(int.parse("$stropacity$stropacity" + strcolor, radix: 16));
  //here color format is 0xFFDDDDDD, where FF is opacity, and DDDDDD is Hex Color
}

List<NavObject> navItems = [
  NavObject(
    screen: NewcategoriePage(),
    navIcon: Icon(Icons.home, size: 30),
    title: Text(''),
  ),
  NavObject(
    screen: ProfilPage(),
    navIcon: Icon(Icons.person, size: 30),
    title: Text(''),
  ),
  NavObject(
    screen: ClassementPage(),
    navIcon: Icon(Icons.wine_bar_sharp, size: 30),
    title: Text(''),
  ),
];

class BottomNav extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BottomNavState();
  }
}

class _BottomNavState extends State<BottomNav> {
  int _screenNumber = 0;
  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        Scaffold(
          body: navItems[_screenNumber].screen,
          bottomNavigationBar: BottomNavigationBar(
            backgroundColor: Color(0xff2bca00),
            selectedItemColor: Colors.white,
            unselectedItemColor: HexaColor("#ffffff", opacity: 85),
            type: BottomNavigationBarType.fixed,
            items: navItems
                .map((navItem) => BottomNavigationBarItem(
              icon: navItem.navIcon,
              label: ""
            ))
                .toList(),
            currentIndex: _screenNumber,
            onTap: (i) => setState(() {
              _screenNumber = i;
            }),
          ),

        )

    );
  }
}