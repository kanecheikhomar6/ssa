import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:ssa_quizz/bottomnavbar.dart';
import 'package:ssa_quizz/preferences/preferences.dart';

import 'Newcategorie.dart';
import 'inscription.dart';
import 'loader.dart';
import 'login.dart';

class FirstPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _FirstPageState();
  }
}

class _FirstPageState extends State<FirstPage> {
   String iduser;
   bool loader = false;
  @override
  void initState() {
    setState(() {
      loader = false;
    });
    Preferences.getUserId().then((value) =>
    {
      if (value == null) {
      } else
        {
          setState(() {
            iduser = value;
            loader = false;
          }),
          print("hhhhhhhhhhhhhhhhhhhhh"),
          print(iduser),
          print("hhhhhhhhhhhhhhhhhhhhh"),
        }
    });
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
          Scaffold(
           body: SingleChildScrollView(child: Column(children:[
             Container(
               height: sizeY*0.55,
               width: sizeX,
               decoration: BoxDecoration(
                 image: DecorationImage(
                     fit: BoxFit.fill,
                     image: AssetImage("assets/images/logo.png")
                 ),
               ),
             ),

             SizedBox(height: 50,),

             // Button se connecter
             GestureDetector(
               onTap: (){
                 Navigator.push(
                   context,
                   MaterialPageRoute(builder: (context) => LoginPage()),
                 );
               },
               child: Container(
               child: // Se connecter
               Center(child: Text(
                   "Se connecter",
                   style: const TextStyle(
                       color:  const Color(0xffffffff),
                       fontWeight: FontWeight.w600,
                       fontFamily: "Rubik",
                       fontStyle:  FontStyle.normal,
                       fontSize: 26.0
                   ),
                   textAlign: TextAlign.left
                 ),
               ),
                 width: sizeX*0.8,
                 height: 45,
                 decoration: BoxDecoration(
                     borderRadius: BorderRadius.all(
                         Radius.circular(10),
                     ),
                     color: const Color(0xff2bca00),
                 ),
             ),),

             SizedBox(height: 20,),
             // Button s'inscrire
             GestureDetector(
                 onTap: (){
                   Navigator.push(
                     context,
                     MaterialPageRoute(builder: (context) => RegisterPage()),
                   );
                 },
                 child: Container(
               child: Center(child: // S’inscrire
               Text(
                   "S’inscrire",
                   style: const TextStyle(
                       color:  const Color(0xffff0000),
                       fontWeight: FontWeight.w600,
                       fontFamily: "Rubik",
                       fontStyle:  FontStyle.normal,
                       fontSize: 26.0
                   ),
                   textAlign: TextAlign.left
               ),),
                 width: sizeX*0.8,
                 height: 45,
                 decoration: BoxDecoration(
                     borderRadius: BorderRadius.all(
                         Radius.circular(10)
                     ),
                     border: Border.all(
                         color: const Color(0xffff0000),
                         width: 2
                     )
                 )
             ),),

             SizedBox(height: 30,),
           ]),
         )
    ));
  }
}
