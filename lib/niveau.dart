import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:ssa_quizz/loader.dart';
import 'package:ssa_quizz/preferences/preferences.dart';
import 'package:ssa_quizz/questionpage.dart';
import 'package:ssa_quizz/quizz.dart';


class NiveauPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NiveauPageState();
  }
}

class _NiveauPageState extends State<NiveauPage> {

  // String email = '', password = '';
  // bool passwordVisible=false;
  // TextEditingController _email = TextEditingController();
  // TextEditingController _password = TextEditingController();
  //
  //
  //
   bool loader = true;
  // bool ok = false;
  // bool clicked = false;
  //
  //
  // void connexion() {
  //
  //   setState(() {
  //     loader=true;
  //     clicked = true;
  //   });
  //
  //   http.post(
  //       Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/login"),
  //       headers: {"Content-Type": "application/json"},
  //       body: json.encode({
  //         "login": email,
  //         "password": password
  //       }))
  //       .then((http.Response response) {
  //     print(response.statusCode);
  //     print(response.body);
  //     var datas = json.decode(response.body);
  //     print(datas["access_token"]);
  //     print("fffffffffffffffffff");
  //     print(datas["user"]["email"]);
  //     print("fffffffffffffffffff");
  //     // voir si le document n'existe pas déjà pour lever cette exception
  //     // To do later
  //     if (response.statusCode == 200) {
  //       Preferences.saveToken(datas["access_token"]);
  //       Preferences.saveUserId(datas["user"]["user_id"]);
  //       Preferences.saveUserNom(datas["user"]["nom_complet"]);
  //       Preferences.saveUserEmail(datas["user"]["email"]);
  //       Preferences.saveUserRole(datas["user"]["role"]);
  //       setState(() {
  //         ok=true;
  //       });
  //       print("ok");
  //     }else{
  //       print("pas bon");
  //       ok=false;
  //     }
  //   });
  // }
   String token;
   var datas;
   var niveaux;
   String cat;


   final FlutterTts flutterTts = FlutterTts();
   speak() async{
     await flutterTts.setLanguage("fr-FR");
     await flutterTts.setPitch(1);
     await flutterTts.speak("Choisissez le niveau de difficulté des questions");
   }


  @override
  void initState() {
     speak();
    setState(() {
      loader=true;
    });
    super.initState();

    Preferences.getCat().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          cat = value;
        }),
        print("hhhhhhhhhhhhhhhhhhhhh"),
        print(cat),
        print("hhhhhhhhhhhhhhhhhhhhh"),
      }
    });

    Preferences.getToken().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          token = value;
        }),
        http.get(
          Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/niveaux"),
          headers: {
            "Content-Type": "application/json",
            "Authorization": "Bearer "+token
          },
        ).then((http.Response response) {
          print(response.body);
          datas = json.decode(response.body);
          print(datas.length);
          print(datas["data"]);
          niveaux=datas["data"];
          setState(() {
            loader=false;
          });
        }),
        print("fffffffffffffffffffff"),
        print(token),
        print("fffffffffffffffffffff")
      }
    });

  }

  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        loader?Loading():Scaffold(
          appBar: PreferredSize(
            preferredSize: Size.fromHeight(190.0), // here the desired height
            child: AppBar(
              automaticallyImplyLeading: false,
              flexibleSpace: Image(
                image: AssetImage('assets/images/logo.png'),
                fit: BoxFit.cover,
              ),

              backgroundColor: Colors.transparent,
            ),
          ),

          body: ListView.separated(
            separatorBuilder: (context, index) => Divider(
              color: Colors.black,
            ),
            itemCount: niveaux.length,
            itemBuilder: (context, index) => GestureDetector(
              onTap: (){
                Preferences.saveNiv(niveaux[index]["niveaux_id"]).then((value) =>
                {
                Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => QuizzPage()),
                ),

                });
              },
              child: Padding(
              padding: EdgeInsets.all(15.0),
              child:  Text("Niveau " + niveaux[index]["nom"].toString()),
            ),),
          ),


        )
    );
  }
}