import 'package:flutter/material.dart';
import './FirstPage.dart';
import 'package:flutter/services.dart';

import 'Newcategorie.dart';
import 'branding.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: BrandingPage(),
      //home: NewcategoriePage(),
    );
  }
}