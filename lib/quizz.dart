import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:ssa_quizz/FirstPage.dart';
import 'package:ssa_quizz/preferences/preferences.dart';
import 'package:toggle_bar/toggle_bar.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'dart:async';
import 'Newcategorie.dart';
import 'bottomnavbar.dart';
import 'package:flutter/services.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';
import 'package:http/http.dart' as http;

import 'loader.dart';

//
// class NavObject {
//   Widget screen;
//   Icon navIcon;
//   Text title;
//   NavObject({this.screen, this.navIcon, this.title});
// }
//
// List<NavObject> navItems = [
//   NavObject(
//     screen: CategoriePage(),
//     navIcon: Icon(Icons.home),
//     title: Text('Home'),
//   ),
//   NavObject(
//     screen: FirstPage(),
//     navIcon: Icon(Icons.settings),
//     title: Text('Settings'),
//   ),
//   NavObject(
//     screen: CategoriePage(),
//     navIcon: Icon(Icons.share),
//     title: Text('Share'),
//   ),
// ];

class QuizzPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _QuizzPageState();
  }
}

class _QuizzPageState extends State<QuizzPage> {
  YoutubePlayerController _controller;


  List<String> labels = ["Q1","Q2","Q3","Q4","Q5","Q6"];
  int counter = 0;
  bool loader =true;
  var datas;
  var questions;
  var cats;
  int note=0;
  Timer timer;
  int start = 180;
  String cat;
  String iduser;
  String niv;
  String token;
  String nomCat;

  bool choix13 = false;
  bool choix12 = false;
  bool choix11 = false;
  bool choix14 = false;

  bool choix23 = false;
  bool choix22 = false;
  bool choix21 = false;
  bool choix24 = false;

  bool choix33 = false;
  bool choix32 = false;
  bool choix31 = false;
  bool choix34 = false;

  bool choix43 = false;
  bool choix42 = false;
  bool choix41 = false;
  bool choix44 = false;

  bool choix54 = false;
  bool choix53 = false;
  bool choix52 = false;
  bool choix51 = false;

  bool choix64 = false;
  bool choix63 = false;
  bool choix62 = false;
  bool choix61 = false;

  bool finish = false;

  bool q1 = true;
  bool q2 = false;
  bool q3 = false;
  bool q4 = false;
  bool q5 = false;
  bool q6 = false;

  int noteq1=0;
  int noteq2=0;
  int noteq3=0;
  int noteq4=0;
  int noteq5=0;
  int noteq6=0;
  int notetotale = 0;


  void startTimer() {
    timer = Timer.periodic(Duration(seconds: 1), (Timer timer) {
      setState(() {
        start--;
        if(start < 150 && start >119){
          q1=false;
          q2=true;
          q3=false;
          q4=false;
          q5=false;
          q6=false;
        }

        if(start < 120 && start >89){
          q1=false;
          q2=false;
          q3=true;
          q4=false;
          q5=false;
          q6=false;
        }

        if(start < 90 && start >59){
          q1=false;
          q2=false;
          q3=false;
          q4=true;
          q5=false;
          q6=false;
        }

        if(start < 60 && start >29){
          q1=false;
          q2=false;
          q3=false;
          q4=false;
          q5=true;
          q6=false;
        }

        if(start < 30 && start >0){
          q1=false;
          q2=false;
          q3=false;
          q4=false;
          q5=false;
          q6=true;
        }

        if(start==0 &&!finish){
          _settingModalBottomSheet(context);
        }

        if(finish && start >0){
          start=0;
        }

        if(start==0){
          finish=true;
          q1=true;
          q2=false;
          q3=false;
          q4=false;
          q5=false;
          q6=false;
          // ignore: unrelated_type_equality_checks
          speak("Correction:"+"Question une"+questions[0]["question"]+"réponses réponses correcte: "+ questions[0]["reponses"][0]["is_true_response"]==true?questions[0]["reponses"][0]["reponse"]:""+","+questions[0]["reponses"][1]["is_true_response"]==true?questions[0]["reponses"][1]["reponse"]:""+","+questions[0]["reponses"][2]["is_true_response"]==true?questions[0]["reponses"][2]["reponse"]:""+","+questions[0]["reponses"][3]["is_true_response"]==true?questions[0]["reponses"][3]["reponse"]:"");
        }

        if(start==177){
          speak("Question."+questions[0]["question"]+"réponses possibles: "+questions[0]["reponses"][0]["reponse"]+","+questions[0]["reponses"][1]["reponse"]+","+questions[0]["reponses"][2]["reponse"]+","+questions[0]["reponses"][3]["reponse"]);
          // speak(questions[0]["reponses"][0]);
          // speak(questions[0]["reponses"][1]);
          // speak(questions[0]["reponses"][2]);
          // speak(questions[0]["reponses"][3]);
        }

        if(start==149){
          speak("Question."+questions[1]["question"]+"réponses possibles: "+questions[1]["reponses"][0]["reponse"]+","+questions[1]["reponses"][1]["reponse"]+","+questions[1]["reponses"][2]["reponse"]+","+questions[1]["reponses"][3]["reponse"]);
          // speak(questions[0]["reponses"][0]);
          // speak(questions[0]["reponses"][1]);
          // speak(questions[0]["reponses"][2]);
          // speak(questions[0]["reponses"][3]);
        }

        if(start==119){
          speak("Question."+questions[2]["question"]+"réponses possibles: "+questions[2]["reponses"][0]["reponse"]+","+questions[2]["reponses"][1]["reponse"]+","+questions[2]["reponses"][2]["reponse"]+","+questions[2]["reponses"][3]["reponse"]);
          // speak(questions[0]["reponses"][0]);
          // speak(questions[0]["reponses"][1]);
          // speak(questions[0]["reponses"][2]);
          // speak(questions[0]["reponses"][3]);
        }

        if(start==89){
          speak("Question."+questions[3]["question"]+"réponses possibles: "+questions[3]["reponses"][0]["reponse"]+","+questions[3]["reponses"][1]["reponse"]+","+questions[3]["reponses"][2]["reponse"]+","+questions[3]["reponses"][3]["reponse"]);
          // speak(questions[0]["reponses"][0]);
          // speak(questions[0]["reponses"][1]);
          // speak(questions[0]["reponses"][2]);
          // speak(questions[0]["reponses"][3]);
        }

        if(start==59){
          speak("Question."+questions[4]["question"]+"réponses possibles: "+questions[4]["reponses"][0]["reponse"]+","+questions[4]["reponses"][1]["reponse"]+","+questions[4]["reponses"][2]["reponse"]+","+questions[4]["reponses"][3]["reponse"]);
          // speak(questions[0]["reponses"][0]);
          // speak(questions[0]["reponses"][1]);
          // speak(questions[0]["reponses"][2]);
          // speak(questions[0]["reponses"][3]);
        }

        if(start==29){
          speak("Question."+questions[5]["question"]+"réponses possibles: "+questions[5]["reponses"][0]["reponse"]+","+questions[5]["reponses"][1]["reponse"]+","+questions[5]["reponses"][2]["reponse"]+","+questions[5]["reponses"][3]["reponse"]);
          // speak(questions[0]["reponses"][0]);
          // speak(questions[0]["reponses"][1]);
          // speak(questions[0]["reponses"][2]);
          // speak(questions[0]["reponses"][3]);
        }

      });
    });
  }



  final FlutterTts flutterTts = FlutterTts();
  speak(a) async{
    await flutterTts.setLanguage("fr-FR");
    await flutterTts.setPitch(1);
    await flutterTts.speak(a);
  }

  @override
  void initState() {

    startTimer();
    _controller = YoutubePlayerController(
      initialVideoId: 't8yCimVcer4', // id youtube video
      flags: YoutubePlayerFlags(
        autoPlay: false,
        mute: false,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
      ),
    );


    Preferences.getCat().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          cat = value;
        }),
        print("hhhhhhhhhhhhhhhhhhhhh"),
        print(cat),
        print("hhhhhhhhhhhhhhhhhhhhh"),
      }
    });

    Preferences.getNiv().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          niv = value;
        }),
        print("hhhhhhhhhhhhhhhhhhhhh"),
        print(niv),
        print("hhhhhhhhhhhhhhhhhhhhh"),
      }
    });

    Preferences.getUserId().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          iduser = value;
        }),
        print("hhhhhhhhhhhhhhhhhhhhh"),
        print(iduser),
        print("hhhhhhhhhhhhhhhhhhhhh"),
      }
    });

    Preferences.getToken().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          token = value;
        }),

        print("fffffffffffffffffffff"),
        print(token),
        print("fffffffffffffffffffff"),
    http.get(
    Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/questions-by-categorie-by-niveaux/"+cat+"/"+niv+"/"+iduser),
    headers: {
    "Content-Type": "application/json",
    "Authorization": "Bearer "+token
    },
    ).then((http.Response response) {
    print(response.body);
    datas = json.decode(response.body);
    print(datas);
    print(datas["data"]);
    questions=datas["data"];
    print("ttttttttttttttttttttttttttttt");
    print(questions[0]["reponses"]);
    print("ttttttttttttttttttttttttttttt");

    setState(() {
    loader=false;
    });
    }),
      }
    });


    // http.get(
    // Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/categories/"+cat),
    // headers: {
    // "Content-Type": "application/json",
    // },
    // ).then((http.Response response) {
    // print(response.body);
    // cats = json.decode(response.body);
    // print(cats);
    // print(cats["data"]);
    // nomCat=cats["data"]["nom"];
    // print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    // print(nomCat);
    // print("zzzzzzzzzzzzzzzzzzzzzzzzzzzzz");
    //
    // setState(() {
    // loader=false;
    // });
    // });

    super.initState();
  }

  void dispose() {
    super.dispose();
  }



  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);

    // if(q1){
    //   speak(questions[0]["question"]);
    // }
    // if(q2){
    //   speak(questions[1]["question"]);
    // }

    // if(q3){
    //   speak(questions[2]["question"]+"Réponses possibles"+questions[2]["reponses"][0]+questions[2]["reponses"][1]+questions[2]["reponses"][2]+questions[2]["reponses"][3]);
    // }
    // if(q4){
    //   speak(questions[3]["question"]+"Réponses possibles"+questions[3]["reponses"][0]+questions[3]["reponses"][1]+questions[3]["reponses"][2]+questions[3]["reponses"][3]);
    // }
    // if(q5){
    //   speak(questions[4]["question"]+"Réponses possibles"+questions[4]["reponses"][0]+questions[4]["reponses"][1]+questions[4]["reponses"][2]+questions[4]["reponses"][3]);
    // }
    // if(q6){
    //   speak(questions[5]["question"]+"Réponses possibles"+questions[5]["reponses"][0]+questions[5]["reponses"][1]+questions[5]["reponses"][2]+questions[5]["reponses"][3]);
    // }

    if(choix11 && questions[0]["reponses"][0]["is_true_response"]==true){
         note+=10;
    }else{
      note=0;
    }
    if(choix12 && questions[0]["reponses"][1]["is_true_response"]==true && choix11 && questions[0]["reponses"][0]["is_true_response"]==true){
        note=20;
    }else{
      if(choix12 && questions[0]["reponses"][1]["is_true_response"]==true && choix11 && questions[0]["reponses"][0]["is_true_response"]==false){
        note = 10;
      }else{
        note=0;
      }
    }
    if(choix13 && questions[0]["reponses"][2]["is_true_response"]==true){
        note=note+10;
    }
    if(choix13 && questions[0]["reponses"][2]["is_true_response"]==false){
      note=note-10;
    }


    if(choix14 && questions[0]["reponses"][3]["is_true_response"]==true){
        note=note+10;
    }
    if(choix14 && questions[0]["reponses"][3]["is_true_response"]==false){
      note=note-10;
    }

    if(choix21 && questions[1]["reponses"][0]["is_true_response"]==true){
        note=note+10;
    }
    if(choix21 && questions[1]["reponses"][0]["is_true_response"]==false){
      note=note-10;
    }


    if(choix22 && questions[1]["reponses"][1]["is_true_response"]==true){
        note=note+10;
    }
    if(choix22 && questions[1]["reponses"][1]["is_true_response"]==false){
      note=note-10;
    }


    if(choix23 && questions[1]["reponses"][2]["is_true_response"]==true){
        note=note+10;
    }
    if(choix23 && questions[1]["reponses"][2]["is_true_response"]==false){
      note=note-10;
    }


    if(choix24 && questions[1]["reponses"][3]["is_true_response"]==true){
        note=note+10;
    }
    if(choix24 && questions[1]["reponses"][3]["is_true_response"]==false){
      note=note-10;
    }

    if(choix31 && questions[2]["reponses"][0]["is_true_response"]==true){
        note=note+10;
    }
    if(choix31 && questions[2]["reponses"][0]["is_true_response"]==false){
      note=note-10;
    }


    if(choix32 && questions[2]["reponses"][1]["is_true_response"]==true){
        note=note+10;
    }
    if(choix32 && questions[2]["reponses"][1]["is_true_response"]==false){
      note=note-10;
    }


    if(choix33 && questions[2]["reponses"][2]["is_true_response"]==true){
        note=note+10;
    }
    if(choix33 && questions[2]["reponses"][2]["is_true_response"]==false){
      note=note-10;
    }


    if(choix34 && questions[2]["reponses"][3]["is_true_response"]==true){
        note=note+10;
    }
    if(choix34 && questions[2]["reponses"][3]["is_true_response"]==false){
      note=note-10;
    }

    if(choix41 && questions[3]["reponses"][0]["is_true_response"]==true){
        note=note+10;
    }
    if(choix41 && questions[3]["reponses"][0]["is_true_response"]==false){
      note=note-10;
    }


    if(choix42 && questions[3]["reponses"][1]["is_true_response"]==true){
        note=note+10;
    }
    if(choix42 && questions[3]["reponses"][1]["is_true_response"]==false){
      note=note-10;
    }


    if(choix43 && questions[3]["reponses"][2]["is_true_response"]==true){
        note=note+10;
    }
    if(choix43 && questions[3]["reponses"][2]["is_true_response"]==false){
      note=note-10;
    }


    if(choix44 && questions[3]["reponses"][3]["is_true_response"]==true){
        note=note+10;
    }
    if(choix44 && questions[3]["reponses"][3]["is_true_response"]==false){
      note=note-10;
    }

    if(choix51 && questions[4]["reponses"][0]["is_true_response"]==true){
        note=note+10;
    }
    if(choix51 && questions[4]["reponses"][0]["is_true_response"]==false){
      note=note-10;
    }


    if(choix52 && questions[4]["reponses"][1]["is_true_response"]==true){
        note=note+10;
    }
    if(choix52 && questions[4]["reponses"][1]["is_true_response"]==false){
      note=note-10;
    }


    if(choix53 && questions[4]["reponses"][2]["is_true_response"]==true){
        note=note+10;
    }
    if(choix53 && questions[4]["reponses"][2]["is_true_response"]==false){
      note=note-10;
    }


    if(choix54 && questions[4]["reponses"][3]["is_true_response"]==true){
        note=note+10;
    }
    if(choix54 && questions[4]["reponses"][3]["is_true_response"]==false){
      note=note-10;
    }

    if(choix61 && questions[5]["reponses"][0]["is_true_response"]==true){
        note=note+10;
    }
    if(choix61 && questions[5]["reponses"][0]["is_true_response"]==false){
      note=note-10;
    }

    if(choix62 && questions[5]["reponses"][1]["is_true_response"]==true){
        note=note+10;
    }
    if(choix62 && questions[5]["reponses"][1]["is_true_response"]==false){
      note=note-10;
    }


    if(choix63 && questions[5]["reponses"][2]["is_true_response"]==true){
        note=note+10;
    }
    if(choix63 && questions[5]["reponses"][2]["is_true_response"]==false){
      note=note-10;
    }


    if(choix64 && questions[5]["reponses"][3]["is_true_response"]==true){
        note=note+10;
    }
    if(choix64 && questions[5]["reponses"][3]["is_true_response"]==false){
      note=note-10;
    }

    return (
        loader?Loading():Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(60.0), // here the desired height
              child: AppBar(
                leading: GestureDetector(
                  onTap: (){
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => BottomNav()),
                    );
                  },
                  child: Icon(Icons.chevron_left, color: Color(0xff676767), size: 30,),),
                centerTitle: true,
                title: Text(
                    "",
                    style: const TextStyle(
                        color:  const Color(0xff676767),
                        fontWeight: FontWeight.w600,
                        fontFamily: "Rubik",
                        fontStyle:  FontStyle.normal,
                        fontSize: 18.0
                    ),
                    textAlign: TextAlign.left
                ),
                automaticallyImplyLeading: false,
                flexibleSpace: Image(
                  image: AssetImage('assets/images/backgroundvierge.png'),
                  fit: BoxFit.cover,
                ),

                backgroundColor: Colors.transparent,
              ),
            ),
            body:  SingleChildScrollView(child: Column(
                    children:[

                      // Center(
                      // child: Container(
                      // width: sizeX,
                      //     child: ToggleBar(
                      //        labels: labels,
                      //         textColor: Color(0xff000000),
                      //         backgroundColor: Color(0xfff0f0f0),
                      //         selectedTabColor: Color(0xff676767),
                      //         selectedTextColor: Colors.white,
                      //         labelTextStyle: TextStyle(
                      //             color:  const Color(0xff000000),
                      //             fontWeight: FontWeight.w400,
                      //             fontFamily: "Rubik",
                      //             fontStyle:  FontStyle.normal,
                      //             fontSize: 16.0
                      //         ),
                      //         onSelectionUpdated: (index){
                      //          setState(() {
                      //                  counter = index;
                      //          });
                      //         },
                      //       ),
                      // ),
                      // ),
                      //
                      SizedBox(height: 30,),

                      Container(
                        child: Center(
                          child: Row(
                            children: [
                              SizedBox(width: 8,),
                              Icon(Icons.access_time, color: Color(0xff707070), size: 20),
                              SizedBox(width: 8,),
                              Text(
                                  start<0 ? "Ecoulé": start.toString()+" "+"sec",
                                  style: const TextStyle(
                                      color:  const Color(0xff676767),
                                      fontWeight: FontWeight.w400,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 16.0
                                  ),
                                  textAlign: TextAlign.left
                              ),

                            ],
                          ),
                        ),
                        width: 120,
                        height: 45,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                                Radius.circular(10)
                            ),
                            border: Border.all(
                                color: const Color(0xff707070),
                                width: 1
                            ),
                            color: const Color(0xffffffff)
                        ),
                      ),

                      SizedBox(height: 30,),

                      Center(
                        child: Container(
                          width: sizeX*0.9,
                          child:  Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    q1 = true;
                                    q2 = false;
                                    q3 = false;
                                    q4 = false;
                                    q5 = false;
                                    q6 = false;
                                  });
                                },
                                child: Container(
                                  child: Center(child:Text(
                                      "Q1",
                                      style: TextStyle(
                                          color: q1 ? Colors.white : Color(0xff000000),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),),
                                  decoration: BoxDecoration(
                                    color: q1 ? Color(0xff676767) : Color(0xfff0f0f0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),),

                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    q1 = false;
                                    q2 = true;
                                    q3 = false;
                                    q4 = false;
                                    q5 = false;
                                    q6 = false;
                                  });
                                },
                                child: Container(
                                  child: Center(child:Text(
                                      "Q2",
                                      style: TextStyle(
                                          color: q2 ? Colors.white : Color(0xff000000),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),),
                                  decoration: BoxDecoration(
                                    color: q2 ? Color(0xff676767) : Color(0xfff0f0f0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),),

                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    q1 = false;
                                    q2 = false;
                                    q3 = true;
                                    q4 = false;
                                    q5 = false;
                                    q6 = false;
                                  });
                                },
                                child: Container(
                                  child: Center(child:Text(
                                      "Q3",
                                      style: TextStyle(
                                          color: q3 ? Colors.white : Color(0xff000000),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),),
                                  decoration: BoxDecoration(
                                    color: q3 ? Color(0xff676767) : Color(0xfff0f0f0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),),

                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    q1 = false;
                                    q2 = false;
                                    q3 = false;
                                    q4 = true;
                                    q5 = false;
                                    q6 = false;
                                  });
                                },
                                child: Container(
                                  child: Center(child:Text(
                                      "Q4",
                                      style: TextStyle(
                                          color: q4 ? Colors.white : Color(0xff000000),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),),
                                  decoration: BoxDecoration(
                                    color: q4 ? Color(0xff676767) : Color(0xfff0f0f0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),),

                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    q1 = false;
                                    q2 = false;
                                    q3 = false;
                                    q4 = false;
                                    q5 = true;
                                    q6 = false;
                                  });
                                },
                                child: Container(
                                  child: Center(child:Text(
                                      "Q5",
                                      style: TextStyle(
                                          color:  q5 ? Colors.white : Color(0xff000000),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),),
                                  decoration: BoxDecoration(
                                    color: q5 ? Color(0xff676767) : Color(0xfff0f0f0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),),

                              GestureDetector(
                                onTap: (){
                                  setState(() {
                                    q1 = false;
                                    q2 = false;
                                    q3 = false;
                                    q4 = false;
                                    q5 = false;
                                    q6 = true;
                                  });
                                },
                                child: Container(
                                  child: Center(child:Text(
                                      "Q6",
                                      style: TextStyle(
                                          color: q6 ? Colors.white : Color(0xff000000),
                                          fontWeight: FontWeight.w400,
                                          fontFamily: "Rubik",
                                          fontStyle:  FontStyle.normal,
                                          fontSize: 16.0
                                      ),
                                      textAlign: TextAlign.left
                                  ),),
                                  decoration: BoxDecoration(
                                    color: q6 ? Color(0xff676767) : Color(0xfff0f0f0),
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  height: 40,
                                  width: 40,
                                ),),



                            ],),
                        ),
                      ),


                      SizedBox(height: 30,),


                      Row(children: [
                        SizedBox(width: 20,), Text(
                            "Question",
                            style: const TextStyle(
                                color:  const Color(0xff676767),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 20.0
                            ),
                            textAlign: TextAlign.left
                        ),
                      ],),

                      SizedBox(height: 15,),

                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: Color(0xfff0f0f0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          width: sizeX*0.9,
                          height: 70,
                          child: Center(
                            child: Text(
                                q1 ? questions[0]["question"]:q2?questions[1]["question"]:q3?questions[2]["question"]:q4 ? questions[3]["question"] : q5 ? questions[4]["question"]: q6 ? questions[5]["question"]:"",
                                style: const TextStyle(
                                    color:  const Color(0xff000000),
                                    fontWeight: FontWeight.w400,
                                    fontFamily: "Rubik",
                                    fontStyle:  FontStyle.normal,
                                    fontSize: 12.0
                                ),
                                textAlign: TextAlign.left
                            ),),
                        ),
                      ),

                      SizedBox(height: 20,),

                      Row(children: [
                        SizedBox(width: 20,), Text(
                            finish?"Correction":"Réponses",
                            style: const TextStyle(
                                color:  const Color(0xff676767),
                                fontWeight: FontWeight.w500,
                                fontFamily: "Rubik",
                                fontStyle:  FontStyle.normal,
                                fontSize: 20.0
                            ),
                            textAlign: TextAlign.left
                        ),
                      ],),

                      SizedBox(height: 20,),







                      //QUESTION 1

                      q1&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix11 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix11?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix11 = !choix11;
                                });
                              },
                              title: Text(questions[0]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  choix11? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix11 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):finish &&q1?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[0]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[0]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[0]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix11 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q1?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q1&&!finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix12 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix12?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 57,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix12 = !choix12;
                                });
                              },
                              title: Text(questions[0]["reponses"][1]["reponse"], style: TextStyle(
                                  color: choix12 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix12 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q1&&finish?
                      Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[0]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[0]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 57,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[0]["reponses"][1]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix12 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),

                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      //SizedBox(height: 20,),
                      q1?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q1&&!finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix13 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix13?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix13 = !choix13;
                                });
                              },
                              title: Text(questions[0]["reponses"][2]["reponse"], style: TextStyle(
                                  color: choix13 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix13 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ): q1&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[0]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[0]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[0]["reponses"][2]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix13 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q1?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q1&&!finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix14 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix14?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix14 = !choix14;
                                });
                              },
                              title: Text(questions[0]["reponses"][3]["reponse"], style: TextStyle(
                                  color: choix14 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix14 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ): q1&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[0]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[0]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[0]["reponses"][3]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix14 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q1&&finish?SizedBox(height: 20,):SizedBox(height: 0.005,),

                      q1&&finish? Container(
                        width: sizeX*0.9,
                        child: YoutubePlayer(
                          controller: YoutubePlayerController(
                            initialVideoId: 't8yCimVcer4', //Add videoID.
                            flags: YoutubePlayerFlags(
                              hideControls: false,
                              controlsVisibleAtStart: true,
                              autoPlay: false,
                              mute: false,
                            ),
                          ),
                          showVideoProgressIndicator: true,
                          progressIndicatorColor: Colors.blue,
                        ),):SizedBox(height: 0.005,),

                      //QUESTION 2

                      q2&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix21 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix21?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix21 = !choix21;
                                });
                              },
                              title: Text(questions[1]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  choix21? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix21 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[1]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[1]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[1]["reponses"][0]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix21 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q2?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q2&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix22 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix22?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix22 = !choix22;
                                });
                              },
                              title: Text(questions[1]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  choix22? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix22 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[1]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[1]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[1]["reponses"][1]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix22 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q2?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q2&&!finish? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix23 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix23?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix23 = !choix23;
                                });
                              },
                              title: Text(questions[1]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  choix23 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix23 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[1]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[1]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[1]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix23 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q2?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q2&&!finish? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix24 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix24?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix24 = !choix24;
                                });
                              },
                              title: Text(questions[1]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  choix24 ? Colors.white : Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix24 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q2&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[1]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[1]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[1]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix24 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q2&&finish?SizedBox(height: 20,):SizedBox(height: 0.005,),

                      q2&&finish? Container(
                        width: sizeX*0.9,
                        child: YoutubePlayer(
                          controller: YoutubePlayerController(
                            initialVideoId: 'Rk2AmJwCIyo', //Add videoID.
                            flags: YoutubePlayerFlags(
                              hideControls: false,
                              controlsVisibleAtStart: true,
                              autoPlay: false,
                              mute: false,
                            ),
                          ),
                          showVideoProgressIndicator: true,
                          progressIndicatorColor: Colors.blue,
                        ),):SizedBox(height: 0.005,),









                      //QUESTION 3

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix31 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix31?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix31 = !choix31;
                                });
                              },
                              title: Text(questions[2]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  choix31? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix31 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[2]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[2]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[2]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  Colors.white ,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix31 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q3?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix32 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix32?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix32 = !choix32;
                                });
                              },
                              title: Text(questions[2]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  choix32? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix32 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[2]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[2]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[2]["reponses"][1]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix32 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q3?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix33 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix33?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix33 = !choix33;
                                });
                              },
                              title: Text(questions[2]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  choix33? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix33 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[2]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[2]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[2]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix33 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),




                      q3?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q3&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix34 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix34?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix34 = !choix34;
                                });
                              },
                              title: Text(questions[2]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  choix34? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix34 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q3&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[2]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[2]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[2]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix34 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      //QUESTION 4

                      q4&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix41 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix41?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 68,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix41 = !choix41;
                                });
                              },
                              title: Text(questions[3]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  choix41? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix41 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q4&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[3]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[3]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 68,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[3]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix41 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q4?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q4&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix42 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix42?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix42 = !choix42;
                                });
                              },
                              title: Text(questions[3]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  choix42 ? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix42 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q4&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[3]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[3]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[3]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix42 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),



                      q4?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q4&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix43 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix43?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix43 = !choix43;
                                });
                              },
                              title: Text(questions[3]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  choix43 ? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix43 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q4&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[3]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[3]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[3]["reponses"][2]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix43 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q4?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q4&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix44 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix44?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix44 = !choix44;
                                });
                              },
                              title: Text(questions[3]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  choix44 ? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix44 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q4&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[3]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[3]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[3]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix44 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),






                      //QUESTION 5

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix51 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix51?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix51 = !choix51;
                                });
                              },
                              title: Text(questions[4]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  choix51? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix51 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[4]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[4]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[4]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix51 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q5?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix52 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix52?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix52 = !choix52;
                                });
                              },
                              title: Text(questions[4]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  choix52? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix52 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[4]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[4]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[4]["reponses"][1]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix52 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q5?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix53 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix53?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix53 = !choix53;
                                });
                              },
                              title: Text(questions[4]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  choix53? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix53 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[4]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[4]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[4]["reponses"][2]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix53 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q5?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q5&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix54 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix54?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix54 = !choix54;
                                });
                              },
                              title: Text(questions[4]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  choix54? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix54 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q5&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[4]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[4]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[4]["reponses"][3]["reponse"], style: TextStyle(
                                  color: Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix54 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),












                      //QUESTION 6

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix61 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix61?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix61 = !choix61;
                                });
                              },
                              title: Text(questions[5]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  choix61? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix61 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[5]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[5]["reponses"][0]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[5]["reponses"][0]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix61 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q6?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix62 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix62?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix62 = !choix62;
                                });
                              },
                              title: Text(questions[5]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  choix62? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix62 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[5]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[5]["reponses"][1]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[5]["reponses"][1]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),
                              ),
                              trailing: choix62 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),

                      q6?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix63 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix63?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix63 = !choix63;
                                });
                              },
                              title: Text(questions[5]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  choix63? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix63 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[5]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[5]["reponses"][2]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[5]["reponses"][2]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix63 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      q6?SizedBox(height: 20,):SizedBox(height: 0.0005,),

                      q6&&!finish ? Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: choix64 ? Color(0xff676767) : Colors.white,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: choix64?Color(0xff676767):Color(0xff676767),
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              onTap: (){
                                setState(() {
                                  choix64 = !choix64;
                                });
                              },
                              title: Text(questions[5]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  choix64? Colors.white :Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix64 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):q6&&finish?Center(
                        child: Container(
                          decoration: BoxDecoration(
                            color: questions[5]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: questions[5]["reponses"][3]["is_true_response"]==true?Color(0xff86c400):Colors.red,
                              width: 1,
                            ),
                          ),
                          width: sizeX*0.9,
                          height: 55,
                          child: Center(
                            child: ListTile(
                              title: Text(questions[5]["reponses"][3]["reponse"], style: TextStyle(
                                  color:  Colors.white,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 12.0
                              ),),
                              trailing: choix64 ?  Icon(Icons.check_box, color: Colors.white,) : Text(""),
                            ),
                          ),
                        ),
                      ):SizedBox(height: 0.5,),


                      SizedBox(height: 25,),


                      q6&&!finish? GestureDetector(
                          onTap: (){
                            setState(() {
                              finish = true;
                              _settingModalBottomSheet(context);
                            });
                            setState(() {
                              loader=true;
                            });
                            http.get(
                              Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/save-gamer-score/"+cat+"/"+niv+"/"+iduser+"/"+note.toString()),
                              headers: {
                                "Content-Type": "application/json",
                                "Authorization": "Bearer "+token
                              },
                            ).then((http.Response response) {
                              print(response.body);
                              datas = json.decode(response.body);

                              print("ttttttttttttttttttttttttttttt");
                              print(datas);
                              print("ttttttttttttttttttttttttttttt");

                              setState(() {
                                loader=false;
                              });
                            });

                          },
                          child: Center(child: Container(
                              child:
                              Center(child: Text(
                                "Terminer",
                                style: const TextStyle(
                                    color:  const Color(0xffffffff),
                                    fontWeight: FontWeight.w600,
                                    fontFamily: "Rubik",
                                    fontStyle:  FontStyle.normal,
                                    fontSize: 30.0
                                ),
                              ),
                              ),
                              width: 285,
                              height: 45,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.all(
                                      Radius.circular(10)
                                  ),
                                  color: const Color(0xff676767)
                              )
                          ))):q6&&finish?SizedBox(height: 1,):SizedBox(height: 1,),


                      q6&&finish? GestureDetector(
                          onTap: (){},
                          child: Center(child: Container(
                              child:
                              Center(child: Text(
                                "Retour",
                                style: const TextStyle(
                                    color:  const Color(0xffffffff),
                                    fontWeight: FontWeight.w600,
                                    fontFamily: "Rubik",
                                    fontStyle:  FontStyle.normal,
                                    fontSize: 30.0
                                ),
                              ),
                              ),
                              width: 285,
                              height: 45,
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.all(
                                    Radius.circular(10)
                                ),
                                color: const Color(0xff676767),
                              )
                          ))):SizedBox(height: 1,),

                      SizedBox(height: 25,),










                    ]),
              )));
  }

  void _settingModalBottomSheet(context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    final text =
        "Pour chaque thème choisi, vous aurez 10 niveaux.\nPour chaque niveau vous aurez 15 questions et 3 coeurs(vies).\n\nSi vous epuisez vos 3 vies pendant la partie, \nune vidéo se lancera automatiquement.\nRegardez-la jusqu'à la fin pour gagner une vie ou\nquittez-la pour terminer la partie.\n\nPour un thème, vous gagnerez une medaille d'or, \nd'argent ou de bronze si vous accomplissez \nles 3 derniers niveaux.";

    showModalBottomSheet(
        isScrollControlled: true,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10.0),
        ),
        backgroundColor: Colors.white,
        context: context,
        builder: (BuildContext bc) {
          return FractionallySizedBox(
              heightFactor: 0.745,
              child: Container(
                height: 900,
                child: new Wrap(
                  children: <Widget>[
                    new ListTile(
                      trailing: new GestureDetector(
                        onTap: () {
                          Navigator.of(context).pop();
                          q1=true;
                          q2=false;
                          q3=false;
                          q4=false;
                          q5=false;
                          q6=false;
                        },
                        child: Icon(
                          Icons.close,
                          size: 30,
                          color: Color(0xff676767),
                        ),
                      ),
                      title: Row(
                        children: [
                          new Text(
                            "Votre score",
                            style: TextStyle(
                              fontSize: 16,
                              color: Color(0xff676767),
                              fontWeight: FontWeight.w700,
                            ),
                          ),
                        ],
                        mainAxisAlignment: MainAxisAlignment.center,
                      ),
                    ),

                    SizedBox(
                      height: 50,
                    ),
                    Center(child:Container(
                      child: Center(child: // 75
                      Text(
                          note.toString(),
                          style: const TextStyle(
                              color:  const Color(0xff676767),
                              fontWeight: FontWeight.w400,
                              fontFamily: "Rubik",
                              fontStyle:  FontStyle.normal,
                              fontSize: 100.0
                          ),
                          textAlign: TextAlign.left
                      ),

                      ),
                      width: 225,
                      height: 135,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(
                              Radius.circular(10)
                          ),
                          border: Border.all(
                              color: const Color(0xff676767),
                              width: 1
                          ),
                      ),
                    ),
                    ),

                    SizedBox(height: 30,),

                    Center(child: Container(
                      // child: Center(child: Row(
                      //   mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      //   children: [
                      //     Container(
                      //       child: Center(
                      //           child: Column(children:[
                      //             Text(
                      //                 "06",
                      //                 style: const TextStyle(
                      //                     color:  const Color(0xff676767),
                      //                     fontWeight: FontWeight.w600,
                      //                     fontFamily: "Rubik",
                      //                     fontStyle:  FontStyle.normal,
                      //                     fontSize: 16.0
                      //                 ),
                      //                 textAlign: TextAlign.left
                      //             ),
                      //
                      //             // Questions
                      //             Text(
                      //                 "Questions",
                      //                 style: const TextStyle(
                      //                     color:  const Color(0xff676767),
                      //                     fontWeight: FontWeight.w400,
                      //                     fontFamily: "Rubik",
                      //                     fontStyle:  FontStyle.normal,
                      //                     fontSize: 10.0
                      //                 ),
                      //                 textAlign: TextAlign.left
                      //             )
                      //           ],)
                      //       ),
                      //       width: 70,
                      //       height: 45,
                      //       decoration: BoxDecoration(
                      //         borderRadius: BorderRadius.all(
                      //             Radius.circular(5)
                      //         ),
                      //         border: Border.all(
                      //             color: const Color(0xff676767),
                      //             width: 1
                      //         ),
                      //       ),
                      //     ),
                      //     Container(
                      //       child: Center(
                      //           child: Column(children:[
                      //             Text(
                      //                 "04",
                      //                 style: TextStyle(
                      //                     color: Colors.white,
                      //                     fontWeight: FontWeight.w600,
                      //                     fontFamily: "Rubik",
                      //                     fontStyle:  FontStyle.normal,
                      //                     fontSize: 16.0
                      //                 ),
                      //                 textAlign: TextAlign.left
                      //             ),
                      //
                      //             // Questions
                      //             Text(
                      //                 "Questions",
                      //                 style: const TextStyle(
                      //                     color:  Colors.white,
                      //                     fontWeight: FontWeight.w400,
                      //                     fontFamily: "Rubik",
                      //                     fontStyle:  FontStyle.normal,
                      //                     fontSize: 10.0
                      //                 ),
                      //                 textAlign: TextAlign.left
                      //             )
                      //           ],)
                      //       ),
                      //       width: 70,
                      //       height: 45,
                      //       decoration: BoxDecoration(
                      //         color: Color(0xff86c400),
                      //         borderRadius: BorderRadius.all(
                      //             Radius.circular(5)
                      //         ),
                      //         border: Border.all(
                      //             color: const Color(0xff86c400),
                      //             width: 1
                      //         ),
                      //       ),
                      //     ),
                      //     Container(
                      //         child: Center(
                      //             child: Column(children:[
                      //               Text(
                      //                   "02",
                      //                   style: TextStyle(
                      //                       color:  Colors.white,
                      //                       fontWeight: FontWeight.w600,
                      //                       fontFamily: "Rubik",
                      //                       fontStyle:  FontStyle.normal,
                      //                       fontSize: 16.0
                      //                   ),
                      //                   textAlign: TextAlign.left
                      //               ),
                      //
                      //               // Questions
                      //               Text(
                      //                   "incorrectes",
                      //                   style: TextStyle(
                      //                       color: Colors.white,
                      //                       fontWeight: FontWeight.w400,
                      //                       fontFamily: "Rubik",
                      //                       fontStyle:  FontStyle.normal,
                      //                       fontSize: 10.0
                      //                   ),
                      //                   textAlign: TextAlign.left
                      //               )
                      //             ],)
                      //         ),
                      //         width: 70,
                      //         height: 45,
                      //         decoration: BoxDecoration(
                      //             color: Color(0xfffb0015),
                      //             borderRadius: BorderRadius.all(
                      //                 Radius.circular(5)
                      //             ),
                      //             border: Border.all(
                      //                 color: const Color(0xfffb0015),
                      //                 width: 1
                      //             ),
                      //         ),
                      //     ),
                      //   ],
                      // ),),
                      height: 120,
                      color: Colors.transparent,
                      width: sizeX*0.8,
                    )),

          GestureDetector(
          onTap: () {
          Navigator.of(context).pop();
          q1=true;
          q2=false;
          q3=false;
          q4=false;
          q5=false;
          q6=false;
          }, child: Center(
                      child:Container(
                          child: Center(
                              child: // Jouer encore
                              Text(
                                  "Correction",
                                  style: const TextStyle(
                                      color:  const Color(0xffffffff),
                                      fontWeight: FontWeight.w600,
                                      fontFamily: "Rubik",
                                      fontStyle:  FontStyle.normal,
                                      fontSize: 26.0
                                  ),
                                  textAlign: TextAlign.left
                              )),
                          width: sizeX*0.75,
                          height: 45,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.all(
                                  Radius.circular(10)
                              ),
                              color: const Color(0xff707070)
                          )
                      ),
                    ),
          ),




                  ],
                ),
              ));
        });
  }


}




