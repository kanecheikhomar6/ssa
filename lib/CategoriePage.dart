import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:ssa_quizz/FirstPage.dart';
import 'package:ssa_quizz/preferences/preferences.dart';
import 'package:ssa_quizz/questionpage.dart';

import 'bottomnavbar.dart';

//
// class NavObject {
//   Widget screen;
//   Icon navIcon;
//   Text title;
//   NavObject({this.screen, this.navIcon, this.title});
// }
//
// List<NavObject> navItems = [
//   NavObject(
//     screen: CategoriePage(),
//     navIcon: Icon(Icons.home),
//     title: Text('Home'),
//   ),
//   NavObject(
//     screen: FirstPage(),
//     navIcon: Icon(Icons.settings),
//     title: Text('Settings'),
//   ),
//   NavObject(
//     screen: CategoriePage(),
//     navIcon: Icon(Icons.share),
//     title: Text('Share'),
//   ),
// ];

class CategoriePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CategoriePageState();
  }
}



class _CategoriePageState extends State<CategoriePage> {

  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        Scaffold(
            appBar: PreferredSize(
                preferredSize: Size.fromHeight(130.0), // here the desired height
                child: AppBar(
                  bottom: PreferredSize(
                      child: Container(
                        color: Colors.transparent,
                        child: Column(
                          children: [
                            Text(
                                "Bonjour!",
                                style: const TextStyle(
                                    color:  const Color(0xff676767),
                                    fontWeight: FontWeight.w400,
                                    fontFamily: "Rubik",
                                    fontStyle:  FontStyle.normal,
                                    fontSize: 16.0
                                ),
                                textAlign: TextAlign.left
                            ),

                            // Mamadou Lamine
                            Text(
                                "Cheikh Omar",
                                style: const TextStyle(
                                    color:  const Color(0xff707070),
                                    fontWeight: FontWeight.w600,
                                    fontFamily: "Rubik",
                                    fontStyle:  FontStyle.normal,
                                    fontSize: 30.0
                                ),
                                textAlign: TextAlign.left
                            ),

                          ],
                        ),
                        height: 70.0,
                      ),
                      preferredSize: Size.fromHeight(4.0)),
                  automaticallyImplyLeading: false,
                  flexibleSpace: Image(
                    image: AssetImage('assets/images/logo_appbar.png'),
                    fit: BoxFit.cover,
                  ),

                  backgroundColor: Colors.transparent,
                ),
            ),
            body: SingleChildScrollView(
              child:
            Container(
              color: Color(0xfff0f0f0),
              child: Column(
                children:[
                  // Container(
                  //   height: 150,
                  //   width: sizeX,
                  //   decoration: BoxDecoration(
                  //     image: DecorationImage(
                  //         fit: BoxFit.fill,
                  //         image: AssetImage("assets/images/logo_appbar.png")
                  //     ),
                  //   ),
                  // ),

                  SizedBox(height: 20,),
                  // Catégories
                  Row(children: [
                    SizedBox(width: 20,), Text(
                        "Catégories",
                        style: const TextStyle(
                            color:  const Color(0xff676767),
                            fontWeight: FontWeight.w500,
                            fontFamily: "Rubik",
                            fontStyle:  FontStyle.normal,
                            fontSize: 20.0
                        ),
                        textAlign: TextAlign.left
                    ),
                  ],),
                  SizedBox(height: 20,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      GestureDetector(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => QuestionPage()),
                          );
                        },
                        child: Column( children:  <Widget>[ Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/personnel.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.amber,
                      ),
                          SizedBox(height: 5,),
                           Text("Hygiéne personnelle",  style: const TextStyle(
                               color:  const Color(0xff0f0f0f),
                               fontWeight: FontWeight.w400,
                               fontFamily: "Rubik",
                               fontStyle:  FontStyle.normal,
                               fontSize: 10.0
                           ),),

        ],
                        ),
                      ),


                      Column( children:  <Widget>[ Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/normes.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.green,
                      ),
                        SizedBox(height: 5,),

                        Text("Normes de SSA",  style: const TextStyle(
                            color:  const Color(0xff0f0f0f),
                            fontWeight: FontWeight.w400,
                            fontFamily: "Rubik",
                            fontStyle:  FontStyle.normal,
                            fontSize: 10.0
                        ),),

                        ],
                      ),
                    ],
                  ),

                  SizedBox(height: 20,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                  Column( children:  <Widget>[ Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/stockage.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.amber,
                      ),
                      SizedBox(height: 5,),


                    Text("Techniques de stockage",  style: const TextStyle(
                        color:  const Color(0xff0f0f0f),
                        fontWeight: FontWeight.w400,
                        fontFamily: "Rubik",
                        fontStyle:  FontStyle.normal,
                        fontSize: 10.0
                    ),),

                  ],),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/prevention.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.green,
                      ),
                    ],
                  ),

                  SizedBox(height: 20,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/normes.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.amber,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/normes.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.green,
                      ),
                    ],
                  ),

                  SizedBox(height: 20,),

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/normes.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.amber,
                      ),
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                            topRight: Radius.circular(30.0),
                            bottomLeft: Radius.circular(30.0),
                          ),
                        ),
                        child: Image(
                          image: AssetImage('assets/images/normes.png'),
                          fit: BoxFit.cover,
                        ),
                        width: sizeX*0.4,
                        height: sizeX*0.4,
                        //color: Colors.green,
                      ),
                    ],
                  ),

                  SizedBox(height: 20,),


            ]),
            ),
        )));
  }
}