import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:ssa_quizz/FirstPage.dart';
import 'package:ssa_quizz/preferences/preferences.dart';
import 'package:ssa_quizz/questionpage.dart';

import 'bottomnavbar.dart';

class BrandingPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _BrandingPageState();
  }
}



class _BrandingPageState extends State<BrandingPage> {

  final FlutterTts flutterTts = FlutterTts();
  speak() async{
    await flutterTts.setLanguage("fr-FR");
    await flutterTts.setPitch(1);
    await flutterTts.speak("L'utilisation de cette application ne crée pas de contrat ou de relation avec l'USAID ou le gouvernement américain. Le gouvernement américain n'assume aucune responsabilité pour les décisions commerciales ou financières prises par l'utilisateur en raison des informations fournies sur cette application. L'utilisation de cette application doit être faite aux seuls risques et responsabilités de l'utilisateur. Les informations fournies via l'application ne peuvent pas être garanties comme étant exactes, opportunes ou précises. Les informations fournies sur cette application ne sont pas des informations officielles du gouvernement des EUA et ne représentent pas le point de vue de l'Agence américaine pour le développement international, USAID, ou du gouvernement américain.");
  }

  @override
  void initState() {
    speak();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        Scaffold(
            body: SingleChildScrollView(

                child: Padding(
            padding: EdgeInsets.symmetric(horizontal: sizeX * .04),
          child:Column(
                    children:[
                      SizedBox(height: 60,),
                      Text("CONDITIONS D'UTILISATION", style: TextStyle(color: Colors.black, fontSize: 20, fontWeight: FontWeight.w900)),
                      SizedBox(height: 25,),
                      Text("L'utilisation de cette application ne crée pas de contrat ou de relation avec l'USAID ou le gouvernement américain. Le gouvernement américain n'assume aucune responsabilité pour les décisions commerciales ou financières prises par l'utilisateur en raison des informations fournies sur cette application. L'utilisation de cette application doit être faite aux seuls risques et responsabilités de l'utilisateur. Les informations fournies via l'application ne peuvent pas être garanties comme étant exactes, opportunes ou précises. \n \n Les informations fournies sur cette application ne sont pas des informations officielles du gouvernement des EUA et ne représentent pas le point de vue de l'Agence américaine pour le développement international, USAID, ou du gouvernement américain.", style: TextStyle(color: Colors.black, fontSize: 20)),
                      SizedBox(height: 25,),
                      GestureDetector(
                        onTap: (){
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => FirstPage()),
                          );
                        },
                        child: Container(
                          child: // Se connecter
                          Center(child: Text(
                              "Accepter",
                              style: const TextStyle(
                                  color:  const Color(0xffffffff),
                                  fontWeight: FontWeight.w600,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 26.0
                              ),
                              textAlign: TextAlign.left
                          ),
                          ),
                          width: sizeX*0.8,
                          height: 45,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.all(
                              Radius.circular(10),
                            ),
                            color: const Color(0xff2bca00),
                          ),
                        ),),
                      SizedBox(height: 25,),
                     ])
,
              )),
            ));
  }
}