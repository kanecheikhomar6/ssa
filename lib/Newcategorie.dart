import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_tts/flutter_tts.dart';
import 'package:persistent_bottom_nav_bar/persistent-tab-view.dart';
import 'package:ssa_quizz/FirstPage.dart';
import 'package:ssa_quizz/niveau.dart';
import 'package:ssa_quizz/preferences/preferences.dart';
import 'package:ssa_quizz/questionpage.dart';
import 'package:http/http.dart' as http;

import 'loader.dart';

class NewcategoriePage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _NewcategoriePageState();
  }
}

class _NewcategoriePageState extends State<NewcategoriePage> {
  String nom;
  String token;
  var datas;
  var categories;
  bool loader=true;

  final FlutterTts flutterTts = FlutterTts();
  speak(a) async{
    await flutterTts.setLanguage("fr-FR");
    await flutterTts.setPitch(1);
    await flutterTts.speak(a);
  }

  @override
  void initState() {
    setState(() {
      loader=true;
    });
    super.initState();
    Preferences.getUserNom().then((value) => {
    if (value == null) {

    } else {
    setState(() {
    //vue = false;
    nom = value;
    })
    }
    });

    Preferences.getToken().then((value) => {
      if (value == null) {

      } else {
        setState(() {
          //vue = false;
          token = value;
        }),
    http.get(
    Uri.parse("https://quizz-sn.withvolkeno.com/api/v1/categories"),
    headers: {
    "Content-Type": "application/json",
    "Authorization": "Bearer "+token
    },
    ).then((http.Response response) {
    print(response.body);
    datas = json.decode(response.body);
    categories=datas["data"];
    print(datas.length);
     setState(() {
       loader=false;
     });
    }),
        print("fffffffffffffffffffff"),
        print(token),
        print("fffffffffffffffffffff")
      }
    });

    speak("Bonjour et Bienvenue sur SSA QUIZZ. Vous pouvez choisir une catégorie");

  }

  @override
  Widget build(BuildContext context) {
    final sizeX = MediaQuery.of(context).size.width;
    final sizeY = MediaQuery.of(context).size.height;
    return (
        loader?Loading():Scaffold(
            appBar: PreferredSize(
              preferredSize: Size.fromHeight(130.0), // here the desired height
              child: AppBar(
                bottom: PreferredSize(
                    child: Container(
                      color: Colors.transparent,
                      child: Column(
                        children: [
                          Text(
                              "Bonjour!",
                              style: const TextStyle(
                                  color:  const Color(0xff676767),
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 16.0
                              ),
                              textAlign: TextAlign.left
                          ),


                          Text(
                              nom==null?'':nom,
                              style: const TextStyle(
                                  color:  const Color(0xff707070),
                                  fontWeight: FontWeight.w600,
                                  fontFamily: "Rubik",
                                  fontStyle:  FontStyle.normal,
                                  fontSize: 30.0
                              ),
                              textAlign: TextAlign.left
                          ),

                        ],
                      ),
                      height: 70.0,
                    ),
                    preferredSize: Size.fromHeight(4.0)),
                automaticallyImplyLeading: false,
                flexibleSpace: Image(
                  image: AssetImage('assets/images/logo_appbar.png'),
                  fit: BoxFit.cover,
                ),

                backgroundColor: Colors.transparent,
              ),
            ),
            body: SingleChildScrollView(child: Padding(
                padding: EdgeInsets.symmetric(horizontal: sizeX * .04),
              child: Column(children: [
                SizedBox(height: 20,),
                GridView.count(
                  primary: false,
                  crossAxisCount: 2,
                  crossAxisSpacing: 2.0,
                  mainAxisSpacing: 2.0,
                  shrinkWrap: true,
                  children: List.generate(
                    //produitsLocaux.toSet().toList().length > 9 &&
                    //!voirPlusLocal
                    //? 9
                    //: produitsLocaux.toSet().toList().length,
                    categories.length,
                        (index) {
                      return GestureDetector(
                        onTap: (){
                          Preferences.saveCat(categories[index]["categorie_id"]);
                          Navigator.push(
                            context,
                            MaterialPageRoute(builder: (context) => NiveauPage()),
                          );
                        },
                        child: Column( children:  <Widget>[ Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30.0),
                              bottomLeft: Radius.circular(30.0),
                            ),
                          ),
                          child: Image.network(
                            'https://quizz-sn.withvolkeno.com'+categories[index]["image"],
                            fit: BoxFit.cover,
                          ),
                          width: sizeX*0.4,
                          height: sizeX*0.4,
                          //color: Colors.amber,
                        ),
                          SizedBox(height: 5,),
                          Text(categories[index]["nom"],  style: const TextStyle(
                              color:  const Color(0xff0f0f0f),
                              fontWeight: FontWeight.w400,
                              fontFamily: "Rubik",
                              fontStyle:  FontStyle.normal,
                              fontSize: 10.0
                          ),),

                        ],
                        ),
                      );
                    },
                  ),
                ),
              ],)
                ),

        )));
  }
}